%YAML 1.1
%TAG !u! tag:unity3d.com,2011:
--- !u!114 &1
MonoBehaviour:
  m_ObjectHideFlags: 52
  m_CorrespondingSourceObject: {fileID: 0}
  m_PrefabInstance: {fileID: 0}
  m_PrefabAsset: {fileID: 0}
  m_GameObject: {fileID: 0}
  m_Enabled: 1
  m_EditorHideFlags: 1
  m_Script: {fileID: 12004, guid: 0000000000000000e000000000000000, type: 0}
  m_Name: 
  m_EditorClassIdentifier: 
  m_PixelRect:
    serializedVersion: 2
    x: 0
    y: 43
    width: 1920
    height: 989
  m_ShowMode: 4
  m_Title: Scene
  m_RootView: {fileID: 10}
  m_MinSize: {x: 875, y: 300}
  m_MaxSize: {x: 10000, y: 10000}
  m_Maximized: 1
--- !u!114 &2
MonoBehaviour:
  m_ObjectHideFlags: 52
  m_CorrespondingSourceObject: {fileID: 0}
  m_PrefabInstance: {fileID: 0}
  m_PrefabAsset: {fileID: 0}
  m_GameObject: {fileID: 0}
  m_Enabled: 1
  m_EditorHideFlags: 0
  m_Script: {fileID: 12004, guid: 0000000000000000e000000000000000, type: 0}
  m_Name: 
  m_EditorClassIdentifier: 
  m_PixelRect:
    serializedVersion: 2
    x: 1056
    y: 125
    width: 694
    height: 855
  m_ShowMode: 0
  m_Title: Package Exporter
  m_RootView: {fileID: 9}
  m_MinSize: {x: 200, y: 100}
  m_MaxSize: {x: 16192, y: 16192}
  m_Maximized: 0
--- !u!114 &3
MonoBehaviour:
  m_ObjectHideFlags: 52
  m_CorrespondingSourceObject: {fileID: 0}
  m_PrefabInstance: {fileID: 0}
  m_PrefabAsset: {fileID: 0}
  m_GameObject: {fileID: 0}
  m_Enabled: 1
  m_EditorHideFlags: 0
  m_Script: {fileID: 12006, guid: 0000000000000000e000000000000000, type: 0}
  m_Name: SceneView
  m_EditorClassIdentifier: 
  m_Children: []
  m_Position:
    serializedVersion: 2
    x: 0
    y: 0
    width: 1920
    height: 939
  m_MinSize: {x: 200, y: 221}
  m_MaxSize: {x: 4000, y: 4021}
  m_ActualView: {fileID: 22}
  m_Panes:
  - {fileID: 22}
  - {fileID: 21}
  - {fileID: 16}
  - {fileID: 15}
  m_Selected: 0
  m_LastSelected: 1
--- !u!114 &4
MonoBehaviour:
  m_ObjectHideFlags: 52
  m_CorrespondingSourceObject: {fileID: 0}
  m_PrefabInstance: {fileID: 0}
  m_PrefabAsset: {fileID: 0}
  m_GameObject: {fileID: 0}
  m_Enabled: 1
  m_EditorHideFlags: 0
  m_Script: {fileID: 12006, guid: 0000000000000000e000000000000000, type: 0}
  m_Name: PackageExporter
  m_EditorClassIdentifier: 
  m_Children: []
  m_Position:
    serializedVersion: 2
    x: 292
    y: 0
    width: 402
    height: 569
  m_MinSize: {x: 101, y: 123}
  m_MaxSize: {x: 4001, y: 4023}
  m_ActualView: {fileID: 14}
  m_Panes:
  - {fileID: 19}
  - {fileID: 14}
  m_Selected: 1
  m_LastSelected: 0
--- !u!114 &5
MonoBehaviour:
  m_ObjectHideFlags: 52
  m_CorrespondingSourceObject: {fileID: 0}
  m_PrefabInstance: {fileID: 0}
  m_PrefabAsset: {fileID: 0}
  m_GameObject: {fileID: 0}
  m_Enabled: 1
  m_EditorHideFlags: 0
  m_Script: {fileID: 12010, guid: 0000000000000000e000000000000000, type: 0}
  m_Name: 
  m_EditorClassIdentifier: 
  m_Children:
  - {fileID: 8}
  - {fileID: 4}
  m_Position:
    serializedVersion: 2
    x: 0
    y: 0
    width: 694
    height: 569
  m_MinSize: {x: 200, y: 50}
  m_MaxSize: {x: 16192, y: 8096}
  vertical: 0
  controlID: 57
--- !u!114 &6
MonoBehaviour:
  m_ObjectHideFlags: 52
  m_CorrespondingSourceObject: {fileID: 0}
  m_PrefabInstance: {fileID: 0}
  m_PrefabAsset: {fileID: 0}
  m_GameObject: {fileID: 0}
  m_Enabled: 1
  m_EditorHideFlags: 0
  m_Script: {fileID: 12006, guid: 0000000000000000e000000000000000, type: 0}
  m_Name: ProjectBrowser
  m_EditorClassIdentifier: 
  m_Children: []
  m_Position:
    serializedVersion: 2
    x: 0
    y: 569
    width: 694
    height: 286
  m_MinSize: {x: 230, y: 269}
  m_MaxSize: {x: 10000, y: 10019}
  m_ActualView: {fileID: 20}
  m_Panes:
  - {fileID: 20}
  - {fileID: 17}
  m_Selected: 0
  m_LastSelected: 1
--- !u!114 &7
MonoBehaviour:
  m_ObjectHideFlags: 52
  m_CorrespondingSourceObject: {fileID: 0}
  m_PrefabInstance: {fileID: 0}
  m_PrefabAsset: {fileID: 0}
  m_GameObject: {fileID: 0}
  m_Enabled: 1
  m_EditorHideFlags: 0
  m_Script: {fileID: 12010, guid: 0000000000000000e000000000000000, type: 0}
  m_Name: 
  m_EditorClassIdentifier: 
  m_Children:
  - {fileID: 5}
  - {fileID: 6}
  m_Position:
    serializedVersion: 2
    x: 0
    y: 0
    width: 694
    height: 855
  m_MinSize: {x: 200, y: 100}
  m_MaxSize: {x: 16192, y: 16192}
  vertical: 1
  controlID: 56
--- !u!114 &8
MonoBehaviour:
  m_ObjectHideFlags: 52
  m_CorrespondingSourceObject: {fileID: 0}
  m_PrefabInstance: {fileID: 0}
  m_PrefabAsset: {fileID: 0}
  m_GameObject: {fileID: 0}
  m_Enabled: 1
  m_EditorHideFlags: 0
  m_Script: {fileID: 12006, guid: 0000000000000000e000000000000000, type: 0}
  m_Name: SceneHierarchyWindow
  m_EditorClassIdentifier: 
  m_Children: []
  m_Position:
    serializedVersion: 2
    x: 0
    y: 0
    width: 292
    height: 569
  m_MinSize: {x: 200, y: 200}
  m_MaxSize: {x: 4000, y: 4000}
  m_ActualView: {fileID: 18}
  m_Panes:
  - {fileID: 18}
  m_Selected: 0
  m_LastSelected: 0
--- !u!114 &9
MonoBehaviour:
  m_ObjectHideFlags: 52
  m_CorrespondingSourceObject: {fileID: 0}
  m_PrefabInstance: {fileID: 0}
  m_PrefabAsset: {fileID: 0}
  m_GameObject: {fileID: 0}
  m_Enabled: 1
  m_EditorHideFlags: 0
  m_Script: {fileID: 12010, guid: 0000000000000000e000000000000000, type: 0}
  m_Name: 
  m_EditorClassIdentifier: 
  m_Children:
  - {fileID: 7}
  m_Position:
    serializedVersion: 2
    x: 0
    y: 0
    width: 694
    height: 855
  m_MinSize: {x: 200, y: 100}
  m_MaxSize: {x: 16192, y: 16192}
  vertical: 0
  controlID: 55
--- !u!114 &10
MonoBehaviour:
  m_ObjectHideFlags: 52
  m_CorrespondingSourceObject: {fileID: 0}
  m_PrefabInstance: {fileID: 0}
  m_PrefabAsset: {fileID: 0}
  m_GameObject: {fileID: 0}
  m_Enabled: 1
  m_EditorHideFlags: 1
  m_Script: {fileID: 12008, guid: 0000000000000000e000000000000000, type: 0}
  m_Name: 
  m_EditorClassIdentifier: 
  m_Children:
  - {fileID: 11}
  - {fileID: 12}
  - {fileID: 13}
  m_Position:
    serializedVersion: 2
    x: 0
    y: 0
    width: 1920
    height: 989
  m_MinSize: {x: 875, y: 300}
  m_MaxSize: {x: 10000, y: 10000}
  m_UseTopView: 1
  m_TopViewHeight: 30
  m_UseBottomView: 1
  m_BottomViewHeight: 20
--- !u!114 &11
MonoBehaviour:
  m_ObjectHideFlags: 52
  m_CorrespondingSourceObject: {fileID: 0}
  m_PrefabInstance: {fileID: 0}
  m_PrefabAsset: {fileID: 0}
  m_GameObject: {fileID: 0}
  m_Enabled: 1
  m_EditorHideFlags: 1
  m_Script: {fileID: 12011, guid: 0000000000000000e000000000000000, type: 0}
  m_Name: 
  m_EditorClassIdentifier: 
  m_Children: []
  m_Position:
    serializedVersion: 2
    x: 0
    y: 0
    width: 1920
    height: 30
  m_MinSize: {x: 0, y: 0}
  m_MaxSize: {x: 0, y: 0}
  m_LastLoadedLayoutName: 
--- !u!114 &12
MonoBehaviour:
  m_ObjectHideFlags: 52
  m_CorrespondingSourceObject: {fileID: 0}
  m_PrefabInstance: {fileID: 0}
  m_PrefabAsset: {fileID: 0}
  m_GameObject: {fileID: 0}
  m_Enabled: 1
  m_EditorHideFlags: 1
  m_Script: {fileID: 12010, guid: 0000000000000000e000000000000000, type: 0}
  m_Name: 
  m_EditorClassIdentifier: 
  m_Children:
  - {fileID: 3}
  m_Position:
    serializedVersion: 2
    x: 0
    y: 30
    width: 1920
    height: 939
  m_MinSize: {x: 100, y: 50}
  m_MaxSize: {x: 8096, y: 8096}
  vertical: 0
  controlID: 17
--- !u!114 &13
MonoBehaviour:
  m_ObjectHideFlags: 52
  m_CorrespondingSourceObject: {fileID: 0}
  m_PrefabInstance: {fileID: 0}
  m_PrefabAsset: {fileID: 0}
  m_GameObject: {fileID: 0}
  m_Enabled: 1
  m_EditorHideFlags: 1
  m_Script: {fileID: 12042, guid: 0000000000000000e000000000000000, type: 0}
  m_Name: 
  m_EditorClassIdentifier: 
  m_Children: []
  m_Position:
    serializedVersion: 2
    x: 0
    y: 969
    width: 1920
    height: 20
  m_MinSize: {x: 0, y: 0}
  m_MaxSize: {x: 0, y: 0}
--- !u!114 &14
MonoBehaviour:
  m_ObjectHideFlags: 52
  m_CorrespondingSourceObject: {fileID: 0}
  m_PrefabInstance: {fileID: 0}
  m_PrefabAsset: {fileID: 0}
  m_GameObject: {fileID: 0}
  m_Enabled: 1
  m_EditorHideFlags: 0
  m_Script: {fileID: 783323549, guid: 52e7e818d0a98674bae61a8fd842187d, type: 3}
  m_Name: 
  m_EditorClassIdentifier: 
  m_MinSize: {x: 100, y: 100}
  m_MaxSize: {x: 4000, y: 4000}
  m_TitleContent:
    m_Text: Package Exporter
    m_Image: {fileID: 0}
    m_Tooltip: 
  m_Pos:
    serializedVersion: 2
    x: 1348
    y: 125
    width: 401
    height: 546
  m_SerializedDataModeController:
    m_DataMode: 0
    m_PreferredDataMode: 0
    m_SupportedDataModes: 
    isAutomatic: 1
  m_ViewDataDictionary: {fileID: 0}
  m_OverlayCanvas:
    m_LastAppliedPresetName: Default
    m_SaveData: []
    m_OverlaysVisible: 1
  exportPackagesList:
  - packageName: PS_CartoonWind
    versionNo: 0.1.0
  - packageName: SD_FogPlane
    versionNo: 0.1.0
  - packageName: PS_FallingLeaves
    versionNo: 0.1.0
  - packageName: PS_GodRay
    versionNo: 0.1.0
--- !u!114 &15
MonoBehaviour:
  m_ObjectHideFlags: 52
  m_CorrespondingSourceObject: {fileID: 0}
  m_PrefabInstance: {fileID: 0}
  m_PrefabAsset: {fileID: 0}
  m_GameObject: {fileID: 0}
  m_Enabled: 1
  m_EditorHideFlags: 0
  m_Script: {fileID: 11500000, guid: 924ffcbe75518854f97b48776d0f1939, type: 3}
  m_Name: 
  m_EditorClassIdentifier: 
  m_MinSize: {x: 100, y: 100}
  m_MaxSize: {x: 4000, y: 4000}
  m_TitleContent:
    m_Text: Fog_Plane_2 (nothing loaded)
    m_Image: {fileID: 2800000, guid: 7129268cf102b2f45809905bcb27ce8b, type: 3}
    m_Tooltip: 
  m_Pos:
    serializedVersion: 2
    x: 0
    y: 73
    width: 1920
    height: 918
  m_SerializedDataModeController:
    m_DataMode: 0
    m_PreferredDataMode: 0
    m_SupportedDataModes: 
    isAutomatic: 1
  m_ViewDataDictionary: {fileID: 0}
  m_OverlayCanvas:
    m_LastAppliedPresetName: Default
    m_SaveData: []
    m_OverlaysVisible: 1
  m_Selected: 6e83d2531d70bad46bb3ef9516c0b5ac
  m_GraphObject: {fileID: 0}
  m_LastSerializedFileContents: "{\n    \"m_SGVersion\": 3,\n    \"m_Type\": \"UnityEditor.ShaderGraph.GraphData\",\n   
    \"m_ObjectId\": \"39b79018c30e4a6bae99825e0ec3739c\",\n    \"m_Properties\":
    [\n        {\n            \"m_Id\": \"3c2bfe7cea0a4a7396ba684bbdeaf155\"\n       
    }\n    ],\n    \"m_Keywords\": [],\n    \"m_Dropdowns\": [],\n    \"m_CategoryData\":
    [\n        {\n            \"m_Id\": \"9073572961f949c097ad86baa9c2ad79\"\n       
    }\n    ],\n    \"m_Nodes\": [\n        {\n            \"m_Id\": \"83b5beb5f58f4f2a847a4f19239419d6\"\n       
    },\n        {\n            \"m_Id\": \"319c59b53f45419089cf403c5e25b531\"\n       
    },\n        {\n            \"m_Id\": \"1ee0fcbdae2648f89a85500eaf181602\"\n       
    },\n        {\n            \"m_Id\": \"baac8694b57f43ae858c4063999f86d5\"\n       
    },\n        {\n            \"m_Id\": \"47d3e83d8e014ecb956d67e369ea37fe\"\n       
    },\n        {\n            \"m_Id\": \"a7b7335a11aa40aa97b94bde0c88c687\"\n       
    },\n        {\n            \"m_Id\": \"05ba53d61b594ba2970b36569517a70f\"\n       
    },\n        {\n            \"m_Id\": \"99b90854d2d241c982f93d97682d7cc7\"\n       
    },\n        {\n            \"m_Id\": \"f867acc8aa524466baaf93a23b8fe22d\"\n       
    },\n        {\n            \"m_Id\": \"1cc062f2156246e9901277132a876c1b\"\n       
    },\n        {\n            \"m_Id\": \"5a78f56cb39f4ea29c30351338035f37\"\n       
    },\n        {\n            \"m_Id\": \"e66a87ef4d48494d9052e474b737aa46\"\n       
    },\n        {\n            \"m_Id\": \"4250a9d826f74ea8b82a5e5d5bb0e5d7\"\n       
    },\n        {\n            \"m_Id\": \"169e9361ee1f480b9e7a9467e9d9959b\"\n       
    },\n        {\n            \"m_Id\": \"02d8f8caa63c45aca92e0ccd483c7ddf\"\n       
    },\n        {\n            \"m_Id\": \"af2374d6328e475abe6e5841029b3445\"\n       
    },\n        {\n            \"m_Id\": \"6ecb130e3f744ec1ad00c388737bd80b\"\n       
    },\n        {\n            \"m_Id\": \"63b2c4d5512446a9ad5c4722e760f033\"\n       
    },\n        {\n            \"m_Id\": \"e5f2b1b42c764f6fb9ea38eeb72a1764\"\n       
    },\n        {\n            \"m_Id\": \"f5b088c7833b4f9eb622fb2970975e0e\"\n       
    },\n        {\n            \"m_Id\": \"828894d04e8648fa9f6bc7b81e1db361\"\n       
    },\n        {\n            \"m_Id\": \"afc85f2148d74310b95d90787bae30a5\"\n       
    }\n    ],\n    \"m_GroupDatas\": [\n        {\n            \"m_Id\": \"ce0298f98f104f44af2de9853c03d209\"\n       
    },\n        {\n            \"m_Id\": \"76209a890b4c4c25bb877c5cca04c8bf\"\n       
    },\n        {\n            \"m_Id\": \"8cbea1a8c24b48049fc7c273d337fda1\"\n       
    },\n        {\n            \"m_Id\": \"979cab19952a45dcb36d4e7e8df32db9\"\n       
    }\n    ],\n    \"m_StickyNoteDatas\": [],\n    \"m_Edges\": [\n        {\n           
    \"m_OutputSlot\": {\n                \"m_Node\": {\n                    \"m_Id\":
    \"02d8f8caa63c45aca92e0ccd483c7ddf\"\n                },\n                \"m_SlotId\":
    2\n            },\n            \"m_InputSlot\": {\n                \"m_Node\":
    {\n                    \"m_Id\": \"af2374d6328e475abe6e5841029b3445\"\n               
    },\n                \"m_SlotId\": 0\n            }\n        },\n        {\n           
    \"m_OutputSlot\": {\n                \"m_Node\": {\n                    \"m_Id\":
    \"05ba53d61b594ba2970b36569517a70f\"\n                },\n                \"m_SlotId\":
    4\n            },\n            \"m_InputSlot\": {\n                \"m_Node\":
    {\n                    \"m_Id\": \"99b90854d2d241c982f93d97682d7cc7\"\n               
    },\n                \"m_SlotId\": 1\n            }\n        },\n        {\n           
    \"m_OutputSlot\": {\n                \"m_Node\": {\n                    \"m_Id\":
    \"169e9361ee1f480b9e7a9467e9d9959b\"\n                },\n                \"m_SlotId\":
    1\n            },\n            \"m_InputSlot\": {\n                \"m_Node\":
    {\n                    \"m_Id\": \"02d8f8caa63c45aca92e0ccd483c7ddf\"\n               
    },\n                \"m_SlotId\": 1\n            }\n        },\n        {\n           
    \"m_OutputSlot\": {\n                \"m_Node\": {\n                    \"m_Id\":
    \"1cc062f2156246e9901277132a876c1b\"\n                },\n                \"m_SlotId\":
    0\n            },\n            \"m_InputSlot\": {\n                \"m_Node\":
    {\n                    \"m_Id\": \"5a78f56cb39f4ea29c30351338035f37\"\n               
    },\n                \"m_SlotId\": 0\n            }\n        },\n        {\n           
    \"m_OutputSlot\": {\n                \"m_Node\": {\n                    \"m_Id\":
    \"4250a9d826f74ea8b82a5e5d5bb0e5d7\"\n                },\n                \"m_SlotId\":
    0\n            },\n            \"m_InputSlot\": {\n                \"m_Node\":
    {\n                    \"m_Id\": \"169e9361ee1f480b9e7a9467e9d9959b\"\n               
    },\n                \"m_SlotId\": 0\n            }\n        },\n        {\n           
    \"m_OutputSlot\": {\n                \"m_Node\": {\n                    \"m_Id\":
    \"47d3e83d8e014ecb956d67e369ea37fe\"\n                },\n                \"m_SlotId\":
    1\n            },\n            \"m_InputSlot\": {\n                \"m_Node\":
    {\n                    \"m_Id\": \"f867acc8aa524466baaf93a23b8fe22d\"\n               
    },\n                \"m_SlotId\": 1\n            }\n        },\n        {\n           
    \"m_OutputSlot\": {\n                \"m_Node\": {\n                    \"m_Id\":
    \"5a78f56cb39f4ea29c30351338035f37\"\n                },\n                \"m_SlotId\":
    2\n            },\n            \"m_InputSlot\": {\n                \"m_Node\":
    {\n                    \"m_Id\": \"e66a87ef4d48494d9052e474b737aa46\"\n               
    },\n                \"m_SlotId\": 0\n            }\n        },\n        {\n           
    \"m_OutputSlot\": {\n                \"m_Node\": {\n                    \"m_Id\":
    \"63b2c4d5512446a9ad5c4722e760f033\"\n                },\n                \"m_SlotId\":
    0\n            },\n            \"m_InputSlot\": {\n                \"m_Node\":
    {\n                    \"m_Id\": \"baac8694b57f43ae858c4063999f86d5\"\n               
    },\n                \"m_SlotId\": 0\n            }\n        },\n        {\n           
    \"m_OutputSlot\": {\n                \"m_Node\": {\n                    \"m_Id\":
    \"63b2c4d5512446a9ad5c4722e760f033\"\n                },\n                \"m_SlotId\":
    0\n            },\n            \"m_InputSlot\": {\n                \"m_Node\":
    {\n                    \"m_Id\": \"f5b088c7833b4f9eb622fb2970975e0e\"\n               
    },\n                \"m_SlotId\": 0\n            }\n        },\n        {\n           
    \"m_OutputSlot\": {\n                \"m_Node\": {\n                    \"m_Id\":
    \"6ecb130e3f744ec1ad00c388737bd80b\"\n                },\n                \"m_SlotId\":
    1\n            },\n            \"m_InputSlot\": {\n                \"m_Node\":
    {\n                    \"m_Id\": \"e5f2b1b42c764f6fb9ea38eeb72a1764\"\n               
    },\n                \"m_SlotId\": 1\n            }\n        },\n        {\n           
    \"m_OutputSlot\": {\n                \"m_Node\": {\n                    \"m_Id\":
    \"99b90854d2d241c982f93d97682d7cc7\"\n                },\n                \"m_SlotId\":
    2\n            },\n            \"m_InputSlot\": {\n                \"m_Node\":
    {\n                    \"m_Id\": \"f867acc8aa524466baaf93a23b8fe22d\"\n               
    },\n                \"m_SlotId\": 0\n            }\n        },\n        {\n           
    \"m_OutputSlot\": {\n                \"m_Node\": {\n                    \"m_Id\":
    \"a7b7335a11aa40aa97b94bde0c88c687\"\n                },\n                \"m_SlotId\":
    0\n            },\n            \"m_InputSlot\": {\n                \"m_Node\":
    {\n                    \"m_Id\": \"05ba53d61b594ba2970b36569517a70f\"\n               
    },\n                \"m_SlotId\": 0\n            }\n        },\n        {\n           
    \"m_OutputSlot\": {\n                \"m_Node\": {\n                    \"m_Id\":
    \"af2374d6328e475abe6e5841029b3445\"\n                },\n                \"m_SlotId\":
    1\n            },\n            \"m_InputSlot\": {\n                \"m_Node\":
    {\n                    \"m_Id\": \"6ecb130e3f744ec1ad00c388737bd80b\"\n               
    },\n                \"m_SlotId\": 0\n            }\n        },\n        {\n           
    \"m_OutputSlot\": {\n                \"m_Node\": {\n                    \"m_Id\":
    \"afc85f2148d74310b95d90787bae30a5\"\n                },\n                \"m_SlotId\":
    0\n            },\n            \"m_InputSlot\": {\n                \"m_Node\":
    {\n                    \"m_Id\": \"99b90854d2d241c982f93d97682d7cc7\"\n               
    },\n                \"m_SlotId\": 0\n            }\n        },\n        {\n           
    \"m_OutputSlot\": {\n                \"m_Node\": {\n                    \"m_Id\":
    \"e5f2b1b42c764f6fb9ea38eeb72a1764\"\n                },\n                \"m_SlotId\":
    2\n            },\n            \"m_InputSlot\": {\n                \"m_Node\":
    {\n                    \"m_Id\": \"828894d04e8648fa9f6bc7b81e1db361\"\n               
    },\n                \"m_SlotId\": 0\n            }\n        },\n        {\n           
    \"m_OutputSlot\": {\n                \"m_Node\": {\n                    \"m_Id\":
    \"e66a87ef4d48494d9052e474b737aa46\"\n                },\n                \"m_SlotId\":
    1\n            },\n            \"m_InputSlot\": {\n                \"m_Node\":
    {\n                    \"m_Id\": \"02d8f8caa63c45aca92e0ccd483c7ddf\"\n               
    },\n                \"m_SlotId\": 0\n            }\n        },\n        {\n           
    \"m_OutputSlot\": {\n                \"m_Node\": {\n                    \"m_Id\":
    \"f5b088c7833b4f9eb622fb2970975e0e\"\n                },\n                \"m_SlotId\":
    4\n            },\n            \"m_InputSlot\": {\n                \"m_Node\":
    {\n                    \"m_Id\": \"e5f2b1b42c764f6fb9ea38eeb72a1764\"\n               
    },\n                \"m_SlotId\": 0\n            }\n        },\n        {\n           
    \"m_OutputSlot\": {\n                \"m_Node\": {\n                    \"m_Id\":
    \"f867acc8aa524466baaf93a23b8fe22d\"\n                },\n                \"m_SlotId\":
    2\n            },\n            \"m_InputSlot\": {\n                \"m_Node\":
    {\n                    \"m_Id\": \"5a78f56cb39f4ea29c30351338035f37\"\n               
    },\n                \"m_SlotId\": 1\n            }\n        }\n    ],\n    \"m_VertexContext\":
    {\n        \"m_Position\": {\n            \"x\": 0.0,\n            \"y\": 0.0\n       
    },\n        \"m_Blocks\": [\n            {\n                \"m_Id\": \"83b5beb5f58f4f2a847a4f19239419d6\"\n           
    },\n            {\n                \"m_Id\": \"319c59b53f45419089cf403c5e25b531\"\n           
    },\n            {\n                \"m_Id\": \"1ee0fcbdae2648f89a85500eaf181602\"\n           
    }\n        ]\n    },\n    \"m_FragmentContext\": {\n        \"m_Position\": {\n           
    \"x\": 0.0,\n            \"y\": 200.0\n        },\n        \"m_Blocks\": [\n           
    {\n                \"m_Id\": \"baac8694b57f43ae858c4063999f86d5\"\n           
    },\n            {\n                \"m_Id\": \"828894d04e8648fa9f6bc7b81e1db361\"\n           
    }\n        ]\n    },\n    \"m_PreviewData\": {\n        \"serializedMesh\": {\n           
    \"m_SerializedMesh\": \"{\\\"mesh\\\":{\\\"instanceID\\\":0}}\",\n           
    \"m_Guid\": \"\"\n        },\n        \"preventRotation\": false\n    },\n   
    \"m_Path\": \"Shader Graphs\",\n    \"m_GraphPrecision\": 1,\n    \"m_PreviewMode\":
    2,\n    \"m_OutputNode\": {\n        \"m_Id\": \"\"\n    },\n    \"m_ActiveTargets\":
    [\n        {\n            \"m_Id\": \"e257204e327d413bb74d15d9de1aaa3d\"\n       
    }\n    ]\n}\n\n{\n    \"m_SGVersion\": 0,\n    \"m_Type\": \"UnityEditor.ShaderGraph.Vector1MaterialSlot\",\n   
    \"m_ObjectId\": \"013ff0520adc4fe6ba0a2be59a678ca3\",\n    \"m_Id\": 3,\n   
    \"m_DisplayName\": \"B\",\n    \"m_SlotType\": 1,\n    \"m_Hidden\": false,\n   
    \"m_ShaderOutputName\": \"B\",\n    \"m_StageCapability\": 3,\n    \"m_Value\":
    0.0,\n    \"m_DefaultValue\": 0.0,\n    \"m_Labels\": []\n}\n\n{\n    \"m_SGVersion\":
    0,\n    \"m_Type\": \"UnityEditor.ShaderGraph.Vector3MaterialSlot\",\n    \"m_ObjectId\":
    \"01d1dcf5644049fe997ff6e8c7dda90a\",\n    \"m_Id\": 0,\n    \"m_DisplayName\":
    \"Position\",\n    \"m_SlotType\": 1,\n    \"m_Hidden\": false,\n    \"m_ShaderOutputName\":
    \"Position\",\n    \"m_StageCapability\": 3,\n    \"m_Value\": {\n        \"x\":
    0.0,\n        \"y\": 0.0,\n        \"z\": 0.0\n    },\n    \"m_DefaultValue\":
    {\n        \"x\": 0.0,\n        \"y\": 0.0,\n        \"z\": 0.0\n    },\n   
    \"m_Labels\": []\n}\n\n{\n    \"m_SGVersion\": 0,\n    \"m_Type\": \"UnityEditor.ShaderGraph.Vector1MaterialSlot\",\n   
    \"m_ObjectId\": \"0253ec4660b544faa5fe48e1a4fd637b\",\n    \"m_Id\": 7,\n   
    \"m_DisplayName\": \"Height\",\n    \"m_SlotType\": 1,\n    \"m_Hidden\": false,\n   
    \"m_ShaderOutputName\": \"Height\",\n    \"m_StageCapability\": 3,\n    \"m_Value\":
    1.0,\n    \"m_DefaultValue\": 1.0,\n    \"m_Labels\": []\n}\n\n{\n    \"m_SGVersion\":
    0,\n    \"m_Type\": \"UnityEditor.ShaderGraph.DotProductNode\",\n    \"m_ObjectId\":
    \"02d8f8caa63c45aca92e0ccd483c7ddf\",\n    \"m_Group\": {\n        \"m_Id\":
    \"979cab19952a45dcb36d4e7e8df32db9\"\n    },\n    \"m_Name\": \"Dot Product\",\n   
    \"m_DrawState\": {\n        \"m_Expanded\": true,\n        \"m_Position\": {\n           
    \"serializedVersion\": \"2\",\n            \"x\": -913.9998779296875,\n           
    \"y\": 299.9999694824219,\n            \"width\": 128.0,\n            \"height\":
    118.00003051757813\n        }\n    },\n    \"m_Slots\": [\n        {\n           
    \"m_Id\": \"87f3808c6da1424385646591a603acfd\"\n        },\n        {\n           
    \"m_Id\": \"2655510b036b4205bd43140e2f8986ca\"\n        },\n        {\n           
    \"m_Id\": \"411ae8c4bc3847aba0372ba6ed213d75\"\n        }\n    ],\n    \"synonyms\":
    [\n        \"scalar product\"\n    ],\n    \"m_Precision\": 0,\n    \"m_PreviewExpanded\":
    false,\n    \"m_DismissedVersion\": 0,\n    \"m_PreviewMode\": 0,\n    \"m_CustomColors\":
    {\n        \"m_SerializableColors\": []\n    }\n}\n\n{\n    \"m_SGVersion\":
    0,\n    \"m_Type\": \"UnityEditor.ShaderGraph.Vector1MaterialSlot\",\n    \"m_ObjectId\":
    \"05481e1ead9f4f87b1a9169160c8ae07\",\n    \"m_Id\": 6,\n    \"m_DisplayName\":
    \"Width\",\n    \"m_SlotType\": 1,\n    \"m_Hidden\": false,\n    \"m_ShaderOutputName\":
    \"Width\",\n    \"m_StageCapability\": 3,\n    \"m_Value\": 1.0,\n    \"m_DefaultValue\":
    1.0,\n    \"m_Labels\": []\n}\n\n{\n    \"m_SGVersion\": 0,\n    \"m_Type\":
    \"UnityEditor.ShaderGraph.SplitNode\",\n    \"m_ObjectId\": \"05ba53d61b594ba2970b36569517a70f\",\n   
    \"m_Group\": {\n        \"m_Id\": \"76209a890b4c4c25bb877c5cca04c8bf\"\n    },\n   
    \"m_Name\": \"Split\",\n    \"m_DrawState\": {\n        \"m_Expanded\": true,\n       
    \"m_Position\": {\n            \"serializedVersion\": \"2\",\n            \"x\":
    -2236.0,\n            \"y\": 376.0,\n            \"width\": 120.0,\n           
    \"height\": 148.99993896484376\n        }\n    },\n    \"m_Slots\": [\n       
    {\n            \"m_Id\": \"1340184b850d465aa5bad7ce2ba0244e\"\n        },\n       
    {\n            \"m_Id\": \"4c797a5f54674bde8bcee9b71b39bd3f\"\n        },\n       
    {\n            \"m_Id\": \"c13dd4368c9545c3b404ec5215cf4ff1\"\n        },\n       
    {\n            \"m_Id\": \"013ff0520adc4fe6ba0a2be59a678ca3\"\n        },\n       
    {\n            \"m_Id\": \"3b9aeddf7e704de78c5fbbca29dc35be\"\n        }\n   
    ],\n    \"synonyms\": [\n        \"separate\"\n    ],\n    \"m_Precision\": 0,\n   
    \"m_PreviewExpanded\": true,\n    \"m_DismissedVersion\": 0,\n    \"m_PreviewMode\":
    0,\n    \"m_CustomColors\": {\n        \"m_SerializableColors\": []\n    }\n}\n\n{\n   
    \"m_SGVersion\": 0,\n    \"m_Type\": \"UnityEditor.ShaderGraph.DynamicVectorMaterialSlot\",\n   
    \"m_ObjectId\": \"07f6eddcc971457c9576e27f7cacf509\",\n    \"m_Id\": 2,\n   
    \"m_DisplayName\": \"Out\",\n    \"m_SlotType\": 1,\n    \"m_Hidden\": false,\n   
    \"m_ShaderOutputName\": \"Out\",\n    \"m_StageCapability\": 3,\n    \"m_Value\":
    {\n        \"x\": 0.0,\n        \"y\": 0.0,\n        \"z\": 0.0,\n        \"w\":
    0.0\n    },\n    \"m_DefaultValue\": {\n        \"x\": 0.0,\n        \"y\": 0.0,\n       
    \"z\": 0.0,\n        \"w\": 0.0\n    }\n}\n\n{\n    \"m_SGVersion\": 0,\n   
    \"m_Type\": \"UnityEditor.ShaderGraph.Vector1MaterialSlot\",\n    \"m_ObjectId\":
    \"0e1554df7f1c4c2db42f8b6ca2819702\",\n    \"m_Id\": 2,\n    \"m_DisplayName\":
    \"G\",\n    \"m_SlotType\": 1,\n    \"m_Hidden\": false,\n    \"m_ShaderOutputName\":
    \"G\",\n    \"m_StageCapability\": 3,\n    \"m_Value\": 0.0,\n    \"m_DefaultValue\":
    0.0,\n    \"m_Labels\": []\n}\n\n{\n    \"m_SGVersion\": 0,\n    \"m_Type\":
    \"UnityEditor.ShaderGraph.DynamicVectorMaterialSlot\",\n    \"m_ObjectId\": \"1340184b850d465aa5bad7ce2ba0244e\",\n   
    \"m_Id\": 0,\n    \"m_DisplayName\": \"In\",\n    \"m_SlotType\": 0,\n    \"m_Hidden\":
    false,\n    \"m_ShaderOutputName\": \"In\",\n    \"m_StageCapability\": 3,\n   
    \"m_Value\": {\n        \"x\": 0.0,\n        \"y\": 0.0,\n        \"z\": 0.0,\n       
    \"w\": 0.0\n    },\n    \"m_DefaultValue\": {\n        \"x\": 0.0,\n        \"y\":
    0.0,\n        \"z\": 0.0,\n        \"w\": 0.0\n    }\n}\n\n{\n    \"m_SGVersion\":
    0,\n    \"m_Type\": \"UnityEditor.ShaderGraph.NormalizeNode\",\n    \"m_ObjectId\":
    \"169e9361ee1f480b9e7a9467e9d9959b\",\n    \"m_Group\": {\n        \"m_Id\":
    \"979cab19952a45dcb36d4e7e8df32db9\"\n    },\n    \"m_Name\": \"Normalize\",\n   
    \"m_DrawState\": {\n        \"m_Expanded\": true,\n        \"m_Position\": {\n           
    \"serializedVersion\": \"2\",\n            \"x\": -1068.9998779296875,\n           
    \"y\": 403.9999084472656,\n            \"width\": 132.0,\n            \"height\":
    94.00003051757813\n        }\n    },\n    \"m_Slots\": [\n        {\n           
    \"m_Id\": \"f6de9e191c0945b5bdf50e51e6b1966a\"\n        },\n        {\n           
    \"m_Id\": \"b9f82f1cb96b4b789df2917ea886abab\"\n        }\n    ],\n    \"synonyms\":
    [],\n    \"m_Precision\": 0,\n    \"m_PreviewExpanded\": false,\n    \"m_DismissedVersion\":
    0,\n    \"m_PreviewMode\": 0,\n    \"m_CustomColors\": {\n        \"m_SerializableColors\":
    []\n    }\n}\n\n{\n    \"m_SGVersion\": 0,\n    \"m_Type\": \"UnityEditor.ShaderGraph.Vector3MaterialSlot\",\n   
    \"m_ObjectId\": \"16b8f677fa7447dfa5c705d6bf0423d3\",\n    \"m_Id\": 1,\n   
    \"m_DisplayName\": \"Direction\",\n    \"m_SlotType\": 1,\n    \"m_Hidden\":
    false,\n    \"m_ShaderOutputName\": \"Direction\",\n    \"m_StageCapability\":
    3,\n    \"m_Value\": {\n        \"x\": 0.0,\n        \"y\": 0.0,\n        \"z\":
    0.0\n    },\n    \"m_DefaultValue\": {\n        \"x\": 0.0,\n        \"y\": 0.0,\n       
    \"z\": 0.0\n    },\n    \"m_Labels\": []\n}\n\n{\n    \"m_SGVersion\": 0,\n   
    \"m_Type\": \"UnityEditor.ShaderGraph.DynamicVectorMaterialSlot\",\n    \"m_ObjectId\":
    \"1c4270d1dc214e1e905126be7f2ca0a4\",\n    \"m_Id\": 2,\n    \"m_DisplayName\":
    \"Out\",\n    \"m_SlotType\": 1,\n    \"m_Hidden\": false,\n    \"m_ShaderOutputName\":
    \"Out\",\n    \"m_StageCapability\": 3,\n    \"m_Value\": {\n        \"x\": 0.0,\n       
    \"y\": 0.0,\n        \"z\": 0.0,\n        \"w\": 0.0\n    },\n    \"m_DefaultValue\":
    {\n        \"x\": 0.0,\n        \"y\": 0.0,\n        \"z\": 0.0,\n        \"w\":
    0.0\n    }\n}\n\n{\n    \"m_SGVersion\": 0,\n    \"m_Type\": \"UnityEditor.ShaderGraph.CameraNode\",\n   
    \"m_ObjectId\": \"1cc062f2156246e9901277132a876c1b\",\n    \"m_Group\": {\n       
    \"m_Id\": \"8cbea1a8c24b48049fc7c273d337fda1\"\n    },\n    \"m_Name\": \"Camera\",\n   
    \"m_DrawState\": {\n        \"m_Expanded\": false,\n        \"m_Position\": {\n           
    \"serializedVersion\": \"2\",\n            \"x\": -1619.9998779296875,\n           
    \"y\": 113.00001525878906,\n            \"width\": 96.9998779296875,\n           
    \"height\": 77.00003051757813\n        }\n    },\n    \"m_Slots\": [\n       
    {\n            \"m_Id\": \"01d1dcf5644049fe997ff6e8c7dda90a\"\n        },\n       
    {\n            \"m_Id\": \"16b8f677fa7447dfa5c705d6bf0423d3\"\n        },\n       
    {\n            \"m_Id\": \"7403b3a93017489c852100e26f1ca4d2\"\n        },\n       
    {\n            \"m_Id\": \"550de9425c3745648d3443a6f527d069\"\n        },\n       
    {\n            \"m_Id\": \"6377ca89f9dc455c942fa9d3b014acb9\"\n        },\n       
    {\n            \"m_Id\": \"90990e4850f2456881632d1d8a819d7d\"\n        },\n       
    {\n            \"m_Id\": \"05481e1ead9f4f87b1a9169160c8ae07\"\n        },\n       
    {\n            \"m_Id\": \"0253ec4660b544faa5fe48e1a4fd637b\"\n        }\n   
    ],\n    \"synonyms\": [\n        \"position\",\n        \"direction\",\n       
    \"orthographic\",\n        \"near plane\",\n        \"far plane\",\n        \"width\",\n       
    \"height\"\n    ],\n    \"m_Precision\": 0,\n    \"m_PreviewExpanded\": true,\n   
    \"m_DismissedVersion\": 0,\n    \"m_PreviewMode\": 0,\n    \"m_CustomColors\":
    {\n        \"m_SerializableColors\": []\n    }\n}\n\n{\n    \"m_SGVersion\":
    0,\n    \"m_Type\": \"UnityEditor.ShaderGraph.BlockNode\",\n    \"m_ObjectId\":
    \"1ee0fcbdae2648f89a85500eaf181602\",\n    \"m_Group\": {\n        \"m_Id\":
    \"\"\n    },\n    \"m_Name\": \"VertexDescription.Tangent\",\n    \"m_DrawState\":
    {\n        \"m_Expanded\": true,\n        \"m_Position\": {\n            \"serializedVersion\":
    \"2\",\n            \"x\": 0.0,\n            \"y\": 0.0,\n            \"width\":
    0.0,\n            \"height\": 0.0\n        }\n    },\n    \"m_Slots\": [\n       
    {\n            \"m_Id\": \"98c3a5c4f507427abed4e6267379ed6b\"\n        }\n   
    ],\n    \"synonyms\": [],\n    \"m_Precision\": 0,\n    \"m_PreviewExpanded\":
    true,\n    \"m_DismissedVersion\": 0,\n    \"m_PreviewMode\": 0,\n    \"m_CustomColors\":
    {\n        \"m_SerializableColors\": []\n    },\n    \"m_SerializedDescriptor\":
    \"VertexDescription.Tangent\"\n}\n\n{\n    \"m_SGVersion\": 0,\n    \"m_Type\":
    \"UnityEditor.ShaderGraph.DynamicVectorMaterialSlot\",\n    \"m_ObjectId\": \"2192fe29bdf747e7bdcd5d5ebb733bea\",\n   
    \"m_Id\": 1,\n    \"m_DisplayName\": \"Out\",\n    \"m_SlotType\": 1,\n    \"m_Hidden\":
    false,\n    \"m_ShaderOutputName\": \"Out\",\n    \"m_StageCapability\": 3,\n   
    \"m_Value\": {\n        \"x\": 0.0,\n        \"y\": 0.0,\n        \"z\": 0.0,\n       
    \"w\": 0.0\n    },\n    \"m_DefaultValue\": {\n        \"x\": 0.0,\n        \"y\":
    0.0,\n        \"z\": 0.0,\n        \"w\": 0.0\n    }\n}\n\n{\n    \"m_SGVersion\":
    0,\n    \"m_Type\": \"UnityEditor.ShaderGraph.DynamicVectorMaterialSlot\",\n   
    \"m_ObjectId\": \"22841751e9b344e391a96b5dba8304a7\",\n    \"m_Id\": 1,\n   
    \"m_DisplayName\": \"B\",\n    \"m_SlotType\": 0,\n    \"m_Hidden\": false,\n   
    \"m_ShaderOutputName\": \"B\",\n    \"m_StageCapability\": 3,\n    \"m_Value\":
    {\n        \"x\": 2.0,\n        \"y\": 2.0,\n        \"z\": 2.0,\n        \"w\":
    2.0\n    },\n    \"m_DefaultValue\": {\n        \"x\": 0.0,\n        \"y\": 0.0,\n       
    \"z\": 0.0,\n        \"w\": 0.0\n    }\n}\n\n{\n    \"m_SGVersion\": 0,\n   
    \"m_Type\": \"UnityEditor.ShaderGraph.DynamicValueMaterialSlot\",\n    \"m_ObjectId\":
    \"2333e30594da400baa82dece2c340147\",\n    \"m_Id\": 2,\n    \"m_DisplayName\":
    \"Out\",\n    \"m_SlotType\": 1,\n    \"m_Hidden\": false,\n    \"m_ShaderOutputName\":
    \"Out\",\n    \"m_StageCapability\": 3,\n    \"m_Value\": {\n        \"e00\":
    0.0,\n        \"e01\": 0.0,\n        \"e02\": 0.0,\n        \"e03\": 0.0,\n       
    \"e10\": 0.0,\n        \"e11\": 0.0,\n        \"e12\": 0.0,\n        \"e13\":
    0.0,\n        \"e20\": 0.0,\n        \"e21\": 0.0,\n        \"e22\": 0.0,\n       
    \"e23\": 0.0,\n        \"e30\": 0.0,\n        \"e31\": 0.0,\n        \"e32\":
    0.0,\n        \"e33\": 0.0\n    },\n    \"m_DefaultValue\": {\n        \"e00\":
    1.0,\n        \"e01\": 0.0,\n        \"e02\": 0.0,\n        \"e03\": 0.0,\n       
    \"e10\": 0.0,\n        \"e11\": 1.0,\n        \"e12\": 0.0,\n        \"e13\":
    0.0,\n        \"e20\": 0.0,\n        \"e21\": 0.0,\n        \"e22\": 1.0,\n       
    \"e23\": 0.0,\n        \"e30\": 0.0,\n        \"e31\": 0.0,\n        \"e32\":
    0.0,\n        \"e33\": 1.0\n    }\n}\n\n{\n    \"m_SGVersion\": 0,\n    \"m_Type\":
    \"UnityEditor.ShaderGraph.DynamicVectorMaterialSlot\",\n    \"m_ObjectId\": \"2655510b036b4205bd43140e2f8986ca\",\n   
    \"m_Id\": 1,\n    \"m_DisplayName\": \"B\",\n    \"m_SlotType\": 0,\n    \"m_Hidden\":
    false,\n    \"m_ShaderOutputName\": \"B\",\n    \"m_StageCapability\": 3,\n   
    \"m_Value\": {\n        \"x\": 0.0,\n        \"y\": 1.0,\n        \"z\": 0.0,\n       
    \"w\": 0.0\n    },\n    \"m_DefaultValue\": {\n        \"x\": 0.0,\n        \"y\":
    0.0,\n        \"z\": 0.0,\n        \"w\": 0.0\n    }\n}\n\n{\n    \"m_SGVersion\":
    0,\n    \"m_Type\": \"UnityEditor.ShaderGraph.Vector3MaterialSlot\",\n    \"m_ObjectId\":
    \"29a2759c37e942b19b96816dedd63d46\",\n    \"m_Id\": 0,\n    \"m_DisplayName\":
    \"Out\",\n    \"m_SlotType\": 1,\n    \"m_Hidden\": false,\n    \"m_ShaderOutputName\":
    \"Out\",\n    \"m_StageCapability\": 3,\n    \"m_Value\": {\n        \"x\": 0.0,\n       
    \"y\": 0.0,\n        \"z\": 0.0\n    },\n    \"m_DefaultValue\": {\n        \"x\":
    0.0,\n        \"y\": 0.0,\n        \"z\": 0.0\n    },\n    \"m_Labels\": []\n}\n\n{\n   
    \"m_SGVersion\": 0,\n    \"m_Type\": \"UnityEditor.ShaderGraph.Vector3MaterialSlot\",\n   
    \"m_ObjectId\": \"2ea26fe573dd4ea3ad36f9f5f360c989\",\n    \"m_Id\": 0,\n   
    \"m_DisplayName\": \"Out\",\n    \"m_SlotType\": 1,\n    \"m_Hidden\": false,\n   
    \"m_ShaderOutputName\": \"Out\",\n    \"m_StageCapability\": 3,\n    \"m_Value\":
    {\n        \"x\": 0.0,\n        \"y\": 0.0,\n        \"z\": 1.0\n    },\n   
    \"m_DefaultValue\": {\n        \"x\": 0.0,\n        \"y\": 0.0,\n        \"z\":
    0.0\n    },\n    \"m_Labels\": []\n}\n\n{\n    \"m_SGVersion\": 0,\n    \"m_Type\":
    \"UnityEditor.ShaderGraph.BlockNode\",\n    \"m_ObjectId\": \"319c59b53f45419089cf403c5e25b531\",\n   
    \"m_Group\": {\n        \"m_Id\": \"\"\n    },\n    \"m_Name\": \"VertexDescription.Normal\",\n   
    \"m_DrawState\": {\n        \"m_Expanded\": true,\n        \"m_Position\": {\n           
    \"serializedVersion\": \"2\",\n            \"x\": 0.0,\n            \"y\": 0.0,\n           
    \"width\": 0.0,\n            \"height\": 0.0\n        }\n    },\n    \"m_Slots\":
    [\n        {\n            \"m_Id\": \"e712508558494429a5b81da9616cc5e2\"\n       
    }\n    ],\n    \"synonyms\": [],\n    \"m_Precision\": 0,\n    \"m_PreviewExpanded\":
    true,\n    \"m_DismissedVersion\": 0,\n    \"m_PreviewMode\": 0,\n    \"m_CustomColors\":
    {\n        \"m_SerializableColors\": []\n    },\n    \"m_SerializedDescriptor\":
    \"VertexDescription.Normal\"\n}\n\n{\n    \"m_SGVersion\": 0,\n    \"m_Type\":
    \"UnityEditor.ShaderGraph.Vector1MaterialSlot\",\n    \"m_ObjectId\": \"3b9aeddf7e704de78c5fbbca29dc35be\",\n   
    \"m_Id\": 4,\n    \"m_DisplayName\": \"A\",\n    \"m_SlotType\": 1,\n    \"m_Hidden\":
    false,\n    \"m_ShaderOutputName\": \"A\",\n    \"m_StageCapability\": 3,\n   
    \"m_Value\": 0.0,\n    \"m_DefaultValue\": 0.0,\n    \"m_Labels\": []\n}\n\n{\n   
    \"m_SGVersion\": 3,\n    \"m_Type\": \"UnityEditor.ShaderGraph.Internal.ColorShaderProperty\",\n   
    \"m_ObjectId\": \"3c2bfe7cea0a4a7396ba684bbdeaf155\",\n    \"m_Guid\": {\n       
    \"m_GuidSerialized\": \"3033cdfa-c6a5-4eea-a121-03236fa7562f\"\n    },\n    \"m_Name\":
    \"Color\",\n    \"m_DefaultRefNameVersion\": 1,\n    \"m_RefNameGeneratedByDisplayName\":
    \"Color\",\n    \"m_DefaultReferenceName\": \"_Color\",\n    \"m_OverrideReferenceName\":
    \"\",\n    \"m_GeneratePropertyBlock\": true,\n    \"m_UseCustomSlotLabel\":
    false,\n    \"m_CustomSlotLabel\": \"\",\n    \"m_DismissedVersion\": 0,\n   
    \"m_Precision\": 0,\n    \"overrideHLSLDeclaration\": false,\n    \"hlslDeclarationOverride\":
    0,\n    \"m_Hidden\": false,\n    \"m_Value\": {\n        \"r\": 0.18431372940540315,\n       
    \"g\": 0.6705882549285889,\n        \"b\": 0.772549033164978,\n        \"a\":
    0.2705882489681244\n    },\n    \"isMainColor\": false,\n    \"m_ColorMode\":
    0\n}\n\n{\n    \"m_SGVersion\": 0,\n    \"m_Type\": \"UnityEditor.ShaderGraph.Vector1MaterialSlot\",\n   
    \"m_ObjectId\": \"411ae8c4bc3847aba0372ba6ed213d75\",\n    \"m_Id\": 2,\n   
    \"m_DisplayName\": \"Out\",\n    \"m_SlotType\": 1,\n    \"m_Hidden\": false,\n   
    \"m_ShaderOutputName\": \"Out\",\n    \"m_StageCapability\": 3,\n    \"m_Value\":
    0.0,\n    \"m_DefaultValue\": 0.0,\n    \"m_Labels\": []\n}\n\n{\n    \"m_SGVersion\":
    0,\n    \"m_Type\": \"UnityEditor.ShaderGraph.DynamicVectorMaterialSlot\",\n   
    \"m_ObjectId\": \"41fd6f88d86d48ec9e1492ed8452d2ee\",\n    \"m_Id\": 1,\n   
    \"m_DisplayName\": \"Out\",\n    \"m_SlotType\": 1,\n    \"m_Hidden\": false,\n   
    \"m_ShaderOutputName\": \"Out\",\n    \"m_StageCapability\": 3,\n    \"m_Value\":
    {\n        \"x\": 0.0,\n        \"y\": 0.0,\n        \"z\": 0.0,\n        \"w\":
    0.0\n    },\n    \"m_DefaultValue\": {\n        \"x\": 0.0,\n        \"y\": 0.0,\n       
    \"z\": 0.0,\n        \"w\": 0.0\n    }\n}\n\n{\n    \"m_SGVersion\": 0,\n   
    \"m_Type\": \"UnityEditor.ShaderGraph.NormalVectorNode\",\n    \"m_ObjectId\":
    \"4250a9d826f74ea8b82a5e5d5bb0e5d7\",\n    \"m_Group\": {\n        \"m_Id\":
    \"979cab19952a45dcb36d4e7e8df32db9\"\n    },\n    \"m_Name\": \"Normal Vector\",\n   
    \"m_DrawState\": {\n        \"m_Expanded\": true,\n        \"m_Position\": {\n           
    \"serializedVersion\": \"2\",\n            \"x\": -1283.9998779296875,\n           
    \"y\": 403.9999084472656,\n            \"width\": 206.0,\n            \"height\":
    131.00009155273438\n        }\n    },\n    \"m_Slots\": [\n        {\n           
    \"m_Id\": \"2ea26fe573dd4ea3ad36f9f5f360c989\"\n        }\n    ],\n    \"synonyms\":
    [\n        \"surface direction\"\n    ],\n    \"m_Precision\": 0,\n    \"m_PreviewExpanded\":
    false,\n    \"m_DismissedVersion\": 0,\n    \"m_PreviewMode\": 2,\n    \"m_CustomColors\":
    {\n        \"m_SerializableColors\": []\n    },\n    \"m_Space\": 0\n}\n\n{\n   
    \"m_SGVersion\": 0,\n    \"m_Type\": \"UnityEditor.ShaderGraph.DynamicValueMaterialSlot\",\n   
    \"m_ObjectId\": \"43db111a153b48099132feee3ee16142\",\n    \"m_Id\": 2,\n   
    \"m_DisplayName\": \"Out\",\n    \"m_SlotType\": 1,\n    \"m_Hidden\": false,\n   
    \"m_ShaderOutputName\": \"Out\",\n    \"m_StageCapability\": 3,\n    \"m_Value\":
    {\n        \"e00\": 0.0,\n        \"e01\": 0.0,\n        \"e02\": 0.0,\n       
    \"e03\": 0.0,\n        \"e10\": 0.0,\n        \"e11\": 0.0,\n        \"e12\":
    0.0,\n        \"e13\": 0.0,\n        \"e20\": 0.0,\n        \"e21\": 0.0,\n       
    \"e22\": 0.0,\n        \"e23\": 0.0,\n        \"e30\": 0.0,\n        \"e31\":
    0.0,\n        \"e32\": 0.0,\n        \"e33\": 0.0\n    },\n    \"m_DefaultValue\":
    {\n        \"e00\": 1.0,\n        \"e01\": 0.0,\n        \"e02\": 0.0,\n       
    \"e03\": 0.0,\n        \"e10\": 0.0,\n        \"e11\": 1.0,\n        \"e12\":
    0.0,\n        \"e13\": 0.0,\n        \"e20\": 0.0,\n        \"e21\": 0.0,\n       
    \"e22\": 1.0,\n        \"e23\": 0.0,\n        \"e30\": 0.0,\n        \"e31\":
    0.0,\n        \"e32\": 0.0,\n        \"e33\": 1.0\n    }\n}\n\n{\n    \"m_SGVersion\":
    0,\n    \"m_Type\": \"UnityEditor.ShaderGraph.SceneDepthNode\",\n    \"m_ObjectId\":
    \"47d3e83d8e014ecb956d67e369ea37fe\",\n    \"m_Group\": {\n        \"m_Id\":
    \"ce0298f98f104f44af2de9853c03d209\"\n    },\n    \"m_Name\": \"Scene Depth\",\n   
    \"m_DrawState\": {\n        \"m_Expanded\": true,\n        \"m_Position\": {\n           
    \"serializedVersion\": \"2\",\n            \"x\": -2262.0,\n            \"y\":
    171.0,\n            \"width\": 145.0,\n            \"height\": 112.0\n       
    }\n    },\n    \"m_Slots\": [\n        {\n            \"m_Id\": \"5c7d9630baf14d868319164c93ef67fd\"\n       
    },\n        {\n            \"m_Id\": \"dd37a4879bc148c79a3b2fd503528185\"\n       
    }\n    ],\n    \"synonyms\": [\n        \"zbuffer\",\n        \"zdepth\"\n   
    ],\n    \"m_Precision\": 0,\n    \"m_PreviewExpanded\": true,\n    \"m_DismissedVersion\":
    0,\n    \"m_PreviewMode\": 0,\n    \"m_CustomColors\": {\n        \"m_SerializableColors\":
    []\n    },\n    \"m_DepthSamplingMode\": 2\n}\n\n{\n    \"m_SGVersion\": 0,\n   
    \"m_Type\": \"UnityEditor.ShaderGraph.DynamicValueMaterialSlot\",\n    \"m_ObjectId\":
    \"484ca351baee4e63aacef3966d1d8c06\",\n    \"m_Id\": 1,\n    \"m_DisplayName\":
    \"B\",\n    \"m_SlotType\": 0,\n    \"m_Hidden\": false,\n    \"m_ShaderOutputName\":
    \"B\",\n    \"m_StageCapability\": 3,\n    \"m_Value\": {\n        \"e00\": 2.0,\n       
    \"e01\": 2.0,\n        \"e02\": 2.0,\n        \"e03\": 2.0,\n        \"e10\":
    2.0,\n        \"e11\": 2.0,\n        \"e12\": 2.0,\n        \"e13\": 2.0,\n       
    \"e20\": 2.0,\n        \"e21\": 2.0,\n        \"e22\": 2.0,\n        \"e23\":
    2.0,\n        \"e30\": 2.0,\n        \"e31\": 2.0,\n        \"e32\": 2.0,\n       
    \"e33\": 2.0\n    },\n    \"m_DefaultValue\": {\n        \"e00\": 1.0,\n       
    \"e01\": 0.0,\n        \"e02\": 0.0,\n        \"e03\": 0.0,\n        \"e10\":
    0.0,\n        \"e11\": 1.0,\n        \"e12\": 0.0,\n        \"e13\": 0.0,\n       
    \"e20\": 0.0,\n        \"e21\": 0.0,\n        \"e22\": 1.0,\n        \"e23\":
    0.0,\n        \"e30\": 0.0,\n        \"e31\": 0.0,\n        \"e32\": 0.0,\n       
    \"e33\": 1.0\n    }\n}\n\n{\n    \"m_SGVersion\": 0,\n    \"m_Type\": \"UnityEditor.ShaderGraph.Vector1MaterialSlot\",\n   
    \"m_ObjectId\": \"4c797a5f54674bde8bcee9b71b39bd3f\",\n    \"m_Id\": 1,\n   
    \"m_DisplayName\": \"R\",\n    \"m_SlotType\": 1,\n    \"m_Hidden\": false,\n   
    \"m_ShaderOutputName\": \"R\",\n    \"m_StageCapability\": 3,\n    \"m_Value\":
    0.0,\n    \"m_DefaultValue\": 0.0,\n    \"m_Labels\": []\n}\n\n{\n    \"m_SGVersion\":
    0,\n    \"m_Type\": \"UnityEditor.ShaderGraph.Vector1MaterialSlot\",\n    \"m_ObjectId\":
    \"550de9425c3745648d3443a6f527d069\",\n    \"m_Id\": 3,\n    \"m_DisplayName\":
    \"Near Plane\",\n    \"m_SlotType\": 1,\n    \"m_Hidden\": false,\n    \"m_ShaderOutputName\":
    \"Near Plane\",\n    \"m_StageCapability\": 3,\n    \"m_Value\": 0.0,\n    \"m_DefaultValue\":
    0.0,\n    \"m_Labels\": []\n}\n\n{\n    \"m_SGVersion\": 0,\n    \"m_Type\":
    \"UnityEditor.ShaderGraph.DynamicVectorMaterialSlot\",\n    \"m_ObjectId\": \"569a499f43864c1fb704c77402eac078\",\n   
    \"m_Id\": 1,\n    \"m_DisplayName\": \"B\",\n    \"m_SlotType\": 0,\n    \"m_Hidden\":
    false,\n    \"m_ShaderOutputName\": \"B\",\n    \"m_StageCapability\": 3,\n   
    \"m_Value\": {\n        \"x\": 1.0,\n        \"y\": 1.0,\n        \"z\": 1.0,\n       
    \"w\": 1.0\n    },\n    \"m_DefaultValue\": {\n        \"x\": 0.0,\n        \"y\":
    0.0,\n        \"z\": 0.0,\n        \"w\": 0.0\n    }\n}\n\n{\n    \"m_SGVersion\":
    0,\n    \"m_Type\": \"UnityEditor.ShaderGraph.SubtractNode\",\n    \"m_ObjectId\":
    \"5a78f56cb39f4ea29c30351338035f37\",\n    \"m_Group\": {\n        \"m_Id\":
    \"8cbea1a8c24b48049fc7c273d337fda1\"\n    },\n    \"m_Name\": \"Subtract\",\n   
    \"m_DrawState\": {\n        \"m_Expanded\": true,\n        \"m_Position\": {\n           
    \"serializedVersion\": \"2\",\n            \"x\": -1499.0,\n            \"y\":
    190.0000457763672,\n            \"width\": 129.9998779296875,\n            \"height\":
    117.99995422363281\n        }\n    },\n    \"m_Slots\": [\n        {\n           
    \"m_Id\": \"9f9787b3946c471da23b98dcc800bc1b\"\n        },\n        {\n           
    \"m_Id\": \"569a499f43864c1fb704c77402eac078\"\n        },\n        {\n           
    \"m_Id\": \"07f6eddcc971457c9576e27f7cacf509\"\n        }\n    ],\n    \"synonyms\":
    [\n        \"subtraction\",\n        \"remove\",\n        \"minus\",\n       
    \"take away\"\n    ],\n    \"m_Precision\": 0,\n    \"m_PreviewExpanded\": false,\n   
    \"m_DismissedVersion\": 0,\n    \"m_PreviewMode\": 0,\n    \"m_CustomColors\":
    {\n        \"m_SerializableColors\": []\n    }\n}\n\n{\n    \"m_SGVersion\":
    0,\n    \"m_Type\": \"UnityEditor.ShaderGraph.ScreenPositionMaterialSlot\",\n   
    \"m_ObjectId\": \"5c7d9630baf14d868319164c93ef67fd\",\n    \"m_Id\": 0,\n   
    \"m_DisplayName\": \"UV\",\n    \"m_SlotType\": 0,\n    \"m_Hidden\": false,\n   
    \"m_ShaderOutputName\": \"UV\",\n    \"m_StageCapability\": 3,\n    \"m_Value\":
    {\n        \"x\": 0.0,\n        \"y\": 0.0,\n        \"z\": 0.0,\n        \"w\":
    0.0\n    },\n    \"m_DefaultValue\": {\n        \"x\": 0.0,\n        \"y\": 0.0,\n       
    \"z\": 0.0,\n        \"w\": 0.0\n    },\n    \"m_Labels\": [],\n    \"m_ScreenSpaceType\":
    0\n}\n\n{\n    \"m_SGVersion\": 0,\n    \"m_Type\": \"UnityEditor.ShaderGraph.DynamicValueMaterialSlot\",\n   
    \"m_ObjectId\": \"5d79c6b0be7a4918a5d10c6d9814e210\",\n    \"m_Id\": 0,\n   
    \"m_DisplayName\": \"A\",\n    \"m_SlotType\": 0,\n    \"m_Hidden\": false,\n   
    \"m_ShaderOutputName\": \"A\",\n    \"m_StageCapability\": 3,\n    \"m_Value\":
    {\n        \"e00\": 0.0,\n        \"e01\": 0.0,\n        \"e02\": 0.0,\n       
    \"e03\": 0.0,\n        \"e10\": 0.0,\n        \"e11\": 0.0,\n        \"e12\":
    0.0,\n        \"e13\": 0.0,\n        \"e20\": 0.0,\n        \"e21\": 0.0,\n       
    \"e22\": 0.0,\n        \"e23\": 0.0,\n        \"e30\": 0.0,\n        \"e31\":
    0.0,\n        \"e32\": 0.0,\n        \"e33\": 0.0\n    },\n    \"m_DefaultValue\":
    {\n        \"e00\": 1.0,\n        \"e01\": 0.0,\n        \"e02\": 0.0,\n       
    \"e03\": 0.0,\n        \"e10\": 0.0,\n        \"e11\": 1.0,\n        \"e12\":
    0.0,\n        \"e13\": 0.0,\n        \"e20\": 0.0,\n        \"e21\": 0.0,\n       
    \"e22\": 1.0,\n        \"e23\": 0.0,\n        \"e30\": 0.0,\n        \"e31\":
    0.0,\n        \"e32\": 0.0,\n        \"e33\": 1.0\n    }\n}\n\n{\n    \"m_SGVersion\":
    0,\n    \"m_Type\": \"UnityEditor.ShaderGraph.Vector1MaterialSlot\",\n    \"m_ObjectId\":
    \"6377ca89f9dc455c942fa9d3b014acb9\",\n    \"m_Id\": 4,\n    \"m_DisplayName\":
    \"Far Plane\",\n    \"m_SlotType\": 1,\n    \"m_Hidden\": false,\n    \"m_ShaderOutputName\":
    \"Far Plane\",\n    \"m_StageCapability\": 3,\n    \"m_Value\": 1.0,\n    \"m_DefaultValue\":
    1.0,\n    \"m_Labels\": []\n}\n\n{\n    \"m_SGVersion\": 0,\n    \"m_Type\":
    \"UnityEditor.ShaderGraph.PropertyNode\",\n    \"m_ObjectId\": \"63b2c4d5512446a9ad5c4722e760f033\",\n   
    \"m_Group\": {\n        \"m_Id\": \"\"\n    },\n    \"m_Name\": \"Property\",\n   
    \"m_DrawState\": {\n        \"m_Expanded\": true,\n        \"m_Position\": {\n           
    \"serializedVersion\": \"2\",\n            \"x\": -414.9999694824219,\n           
    \"y\": 242.0,\n            \"width\": 105.00003051757813,\n            \"height\":
    34.000030517578128\n        }\n    },\n    \"m_Slots\": [\n        {\n           
    \"m_Id\": \"f2d0b4518d69453cae8127d457fa12e7\"\n        }\n    ],\n    \"synonyms\":
    [],\n    \"m_Precision\": 0,\n    \"m_PreviewExpanded\": true,\n    \"m_DismissedVersion\":
    0,\n    \"m_PreviewMode\": 0,\n    \"m_CustomColors\": {\n        \"m_SerializableColors\":
    []\n    },\n    \"m_Property\": {\n        \"m_Id\": \"3c2bfe7cea0a4a7396ba684bbdeaf155\"\n   
    }\n}\n\n{\n    \"m_SGVersion\": 0,\n    \"m_Type\": \"UnityEditor.ShaderGraph.SaturateNode\",\n   
    \"m_ObjectId\": \"6ecb130e3f744ec1ad00c388737bd80b\",\n    \"m_Group\": {\n       
    \"m_Id\": \"979cab19952a45dcb36d4e7e8df32db9\"\n    },\n    \"m_Name\": \"Saturate\",\n   
    \"m_DrawState\": {\n        \"m_Expanded\": true,\n        \"m_Position\": {\n           
    \"serializedVersion\": \"2\",\n            \"x\": -587.0,\n            \"y\":
    300.0,\n            \"width\": 128.0,\n            \"height\": 94.0\n       
    }\n    },\n    \"m_Slots\": [\n        {\n            \"m_Id\": \"b688ce36e65d40f78539a118af04d50d\"\n       
    },\n        {\n            \"m_Id\": \"2192fe29bdf747e7bdcd5d5ebb733bea\"\n       
    }\n    ],\n    \"synonyms\": [\n        \"clamp\"\n    ],\n    \"m_Precision\":
    0,\n    \"m_PreviewExpanded\": false,\n    \"m_DismissedVersion\": 0,\n    \"m_PreviewMode\":
    0,\n    \"m_CustomColors\": {\n        \"m_SerializableColors\": []\n    }\n}\n\n{\n   
    \"m_SGVersion\": 0,\n    \"m_Type\": \"UnityEditor.ShaderGraph.Vector1MaterialSlot\",\n   
    \"m_ObjectId\": \"6f9df22cbaef4542aeff74ed0045c7f0\",\n    \"m_Id\": 1,\n   
    \"m_DisplayName\": \"R\",\n    \"m_SlotType\": 1,\n    \"m_Hidden\": false,\n   
    \"m_ShaderOutputName\": \"R\",\n    \"m_StageCapability\": 3,\n    \"m_Value\":
    0.0,\n    \"m_DefaultValue\": 0.0,\n    \"m_Labels\": []\n}\n\n{\n    \"m_SGVersion\":
    0,\n    \"m_Type\": \"UnityEditor.ShaderGraph.Vector1MaterialSlot\",\n    \"m_ObjectId\":
    \"7403b3a93017489c852100e26f1ca4d2\",\n    \"m_Id\": 2,\n    \"m_DisplayName\":
    \"Orthographic\",\n    \"m_SlotType\": 1,\n    \"m_Hidden\": false,\n    \"m_ShaderOutputName\":
    \"Orthographic\",\n    \"m_StageCapability\": 3,\n    \"m_Value\": 0.0,\n   
    \"m_DefaultValue\": 0.0,\n    \"m_Labels\": []\n}\n\n{\n    \"m_SGVersion\":
    0,\n    \"m_Type\": \"UnityEditor.ShaderGraph.GroupData\",\n    \"m_ObjectId\":
    \"76209a890b4c4c25bb877c5cca04c8bf\",\n    \"m_Title\": \"Object Depth\",\n   
    \"m_Position\": {\n        \"x\": -2454.999755859375,\n        \"y\": 317.0\n   
    }\n}\n\n{\n    \"m_SGVersion\": 0,\n    \"m_Type\": \"UnityEditor.ShaderGraph.DynamicVectorMaterialSlot\",\n   
    \"m_ObjectId\": \"7c05a238379c429aa44d044507009474\",\n    \"m_Id\": 0,\n   
    \"m_DisplayName\": \"In\",\n    \"m_SlotType\": 0,\n    \"m_Hidden\": false,\n   
    \"m_ShaderOutputName\": \"In\",\n    \"m_StageCapability\": 3,\n    \"m_Value\":
    {\n        \"x\": 0.0,\n        \"y\": 0.0,\n        \"z\": 0.0,\n        \"w\":
    0.0\n    },\n    \"m_DefaultValue\": {\n        \"x\": 0.0,\n        \"y\": 0.0,\n       
    \"z\": 0.0,\n        \"w\": 0.0\n    }\n}\n\n{\n    \"m_SGVersion\": 0,\n   
    \"m_Type\": \"UnityEditor.ShaderGraph.DynamicValueMaterialSlot\",\n    \"m_ObjectId\":
    \"7d2b94fa53cb48be93caf642f0bd532f\",\n    \"m_Id\": 1,\n    \"m_DisplayName\":
    \"B\",\n    \"m_SlotType\": 0,\n    \"m_Hidden\": false,\n    \"m_ShaderOutputName\":
    \"B\",\n    \"m_StageCapability\": 3,\n    \"m_Value\": {\n        \"e00\": 2.0,\n       
    \"e01\": 2.0,\n        \"e02\": 2.0,\n        \"e03\": 2.0,\n        \"e10\":
    2.0,\n        \"e11\": 2.0,\n        \"e12\": 2.0,\n        \"e13\": 2.0,\n       
    \"e20\": 2.0,\n        \"e21\": 2.0,\n        \"e22\": 2.0,\n        \"e23\":
    2.0,\n        \"e30\": 2.0,\n        \"e31\": 2.0,\n        \"e32\": 2.0,\n       
    \"e33\": 2.0\n    },\n    \"m_DefaultValue\": {\n        \"e00\": 1.0,\n       
    \"e01\": 0.0,\n        \"e02\": 0.0,\n        \"e03\": 0.0,\n        \"e10\":
    0.0,\n        \"e11\": 1.0,\n        \"e12\": 0.0,\n        \"e13\": 0.0,\n       
    \"e20\": 0.0,\n        \"e21\": 0.0,\n        \"e22\": 1.0,\n        \"e23\":
    0.0,\n        \"e30\": 0.0,\n        \"e31\": 0.0,\n        \"e32\": 0.0,\n       
    \"e33\": 1.0\n    }\n}\n\n{\n    \"m_SGVersion\": 0,\n    \"m_Type\": \"UnityEditor.ShaderGraph.BlockNode\",\n   
    \"m_ObjectId\": \"828894d04e8648fa9f6bc7b81e1db361\",\n    \"m_Group\": {\n       
    \"m_Id\": \"\"\n    },\n    \"m_Name\": \"SurfaceDescription.Alpha\",\n    \"m_DrawState\":
    {\n        \"m_Expanded\": true,\n        \"m_Position\": {\n            \"serializedVersion\":
    \"2\",\n            \"x\": 0.0,\n            \"y\": 0.0,\n            \"width\":
    0.0,\n            \"height\": 0.0\n        }\n    },\n    \"m_Slots\": [\n       
    {\n            \"m_Id\": \"cbc22be1e5fe47358368936e163178a2\"\n        }\n   
    ],\n    \"synonyms\": [],\n    \"m_Precision\": 0,\n    \"m_PreviewExpanded\":
    true,\n    \"m_DismissedVersion\": 0,\n    \"m_PreviewMode\": 0,\n    \"m_CustomColors\":
    {\n        \"m_SerializableColors\": []\n    },\n    \"m_SerializedDescriptor\":
    \"SurfaceDescription.Alpha\"\n}\n\n{\n    \"m_SGVersion\": 0,\n    \"m_Type\":
    \"UnityEditor.ShaderGraph.BlockNode\",\n    \"m_ObjectId\": \"83b5beb5f58f4f2a847a4f19239419d6\",\n   
    \"m_Group\": {\n        \"m_Id\": \"\"\n    },\n    \"m_Name\": \"VertexDescription.Position\",\n   
    \"m_DrawState\": {\n        \"m_Expanded\": true,\n        \"m_Position\": {\n           
    \"serializedVersion\": \"2\",\n            \"x\": 0.0,\n            \"y\": 0.0,\n           
    \"width\": 0.0,\n            \"height\": 0.0\n        }\n    },\n    \"m_Slots\":
    [\n        {\n            \"m_Id\": \"e8f58791e8e343899c681682fd1030d7\"\n       
    }\n    ],\n    \"synonyms\": [],\n    \"m_Precision\": 0,\n    \"m_PreviewExpanded\":
    true,\n    \"m_DismissedVersion\": 0,\n    \"m_PreviewMode\": 0,\n    \"m_CustomColors\":
    {\n        \"m_SerializableColors\": []\n    },\n    \"m_SerializedDescriptor\":
    \"VertexDescription.Position\"\n}\n\n{\n    \"m_SGVersion\": 0,\n    \"m_Type\":
    \"UnityEditor.ShaderGraph.DynamicVectorMaterialSlot\",\n    \"m_ObjectId\": \"87f3808c6da1424385646591a603acfd\",\n   
    \"m_Id\": 0,\n    \"m_DisplayName\": \"A\",\n    \"m_SlotType\": 0,\n    \"m_Hidden\":
    false,\n    \"m_ShaderOutputName\": \"A\",\n    \"m_StageCapability\": 3,\n   
    \"m_Value\": {\n        \"x\": 0.0,\n        \"y\": 0.0,\n        \"z\": 0.0,\n       
    \"w\": 0.0\n    },\n    \"m_DefaultValue\": {\n        \"x\": 0.0,\n        \"y\":
    0.0,\n        \"z\": 0.0,\n        \"w\": 0.0\n    }\n}\n\n{\n    \"m_SGVersion\":
    0,\n    \"m_Type\": \"UnityEditor.ShaderGraph.GroupData\",\n    \"m_ObjectId\":
    \"8cbea1a8c24b48049fc7c273d337fda1\",\n    \"m_Title\": \"Calculate World Pos
    from Depth\",\n    \"m_Position\": {\n        \"x\": -2065.0,\n        \"y\":
    54.0\n    }\n}\n\n{\n    \"m_SGVersion\": 0,\n    \"m_Type\": \"UnityEditor.ShaderGraph.CategoryData\",\n   
    \"m_ObjectId\": \"9073572961f949c097ad86baa9c2ad79\",\n    \"m_Name\": \"\",\n   
    \"m_ChildObjectList\": [\n        {\n            \"m_Id\": \"3c2bfe7cea0a4a7396ba684bbdeaf155\"\n       
    }\n    ]\n}\n\n{\n    \"m_SGVersion\": 0,\n    \"m_Type\": \"UnityEditor.ShaderGraph.Vector1MaterialSlot\",\n   
    \"m_ObjectId\": \"90990e4850f2456881632d1d8a819d7d\",\n    \"m_Id\": 5,\n   
    \"m_DisplayName\": \"Z Buffer Sign\",\n    \"m_SlotType\": 1,\n    \"m_Hidden\":
    false,\n    \"m_ShaderOutputName\": \"Z Buffer Sign\",\n    \"m_StageCapability\":
    3,\n    \"m_Value\": 1.0,\n    \"m_DefaultValue\": 1.0,\n    \"m_Labels\": []\n}\n\n{\n   
    \"m_SGVersion\": 0,\n    \"m_Type\": \"UnityEditor.ShaderGraph.DynamicValueMaterialSlot\",\n   
    \"m_ObjectId\": \"966bdaf3116e410a8ca3110dcfa37dfe\",\n    \"m_Id\": 0,\n   
    \"m_DisplayName\": \"A\",\n    \"m_SlotType\": 0,\n    \"m_Hidden\": false,\n   
    \"m_ShaderOutputName\": \"A\",\n    \"m_StageCapability\": 3,\n    \"m_Value\":
    {\n        \"e00\": 0.0,\n        \"e01\": 0.0,\n        \"e02\": 0.0,\n       
    \"e03\": 0.0,\n        \"e10\": 0.0,\n        \"e11\": 0.0,\n        \"e12\":
    0.0,\n        \"e13\": 0.0,\n        \"e20\": 0.0,\n        \"e21\": 0.0,\n       
    \"e22\": 0.0,\n        \"e23\": 0.0,\n        \"e30\": 0.0,\n        \"e31\":
    0.0,\n        \"e32\": 0.0,\n        \"e33\": 0.0\n    },\n    \"m_DefaultValue\":
    {\n        \"e00\": 1.0,\n        \"e01\": 0.0,\n        \"e02\": 0.0,\n       
    \"e03\": 0.0,\n        \"e10\": 0.0,\n        \"e11\": 1.0,\n        \"e12\":
    0.0,\n        \"e13\": 0.0,\n        \"e20\": 0.0,\n        \"e21\": 0.0,\n       
    \"e22\": 1.0,\n        \"e23\": 0.0,\n        \"e30\": 0.0,\n        \"e31\":
    0.0,\n        \"e32\": 0.0,\n        \"e33\": 1.0\n    }\n}\n\n{\n    \"m_SGVersion\":
    0,\n    \"m_Type\": \"UnityEditor.ShaderGraph.GroupData\",\n    \"m_ObjectId\":
    \"979cab19952a45dcb36d4e7e8df32db9\",\n    \"m_Title\": \"Handle Fog (base on
    Normal Direction)\",\n    \"m_Position\": {\n        \"x\": -1315.0001220703125,\n       
    \"y\": 216.0\n    }\n}\n\n{\n    \"m_SGVersion\": 0,\n    \"m_Type\": \"UnityEditor.ShaderGraph.TangentMaterialSlot\",\n   
    \"m_ObjectId\": \"98c3a5c4f507427abed4e6267379ed6b\",\n    \"m_Id\": 0,\n   
    \"m_DisplayName\": \"Tangent\",\n    \"m_SlotType\": 0,\n    \"m_Hidden\": false,\n   
    \"m_ShaderOutputName\": \"Tangent\",\n    \"m_StageCapability\": 1,\n    \"m_Value\":
    {\n        \"x\": 0.0,\n        \"y\": 0.0,\n        \"z\": 0.0\n    },\n   
    \"m_DefaultValue\": {\n        \"x\": 0.0,\n        \"y\": 0.0,\n        \"z\":
    0.0\n    },\n    \"m_Labels\": [],\n    \"m_Space\": 0\n}\n\n{\n    \"m_SGVersion\":
    0,\n    \"m_Type\": \"UnityEditor.ShaderGraph.DivideNode\",\n    \"m_ObjectId\":
    \"99b90854d2d241c982f93d97682d7cc7\",\n    \"m_Group\": {\n        \"m_Id\":
    \"8cbea1a8c24b48049fc7c273d337fda1\"\n    },\n    \"m_Name\": \"Divide\",\n   
    \"m_DrawState\": {\n        \"m_Expanded\": true,\n        \"m_Position\": {\n           
    \"serializedVersion\": \"2\",\n            \"x\": -1805.0,\n            \"y\":
    406.9999694824219,\n            \"width\": 130.0,\n            \"height\": 117.99996948242188\n       
    }\n    },\n    \"m_Slots\": [\n        {\n            \"m_Id\": \"fd24c513279946d094961ef948751029\"\n       
    },\n        {\n            \"m_Id\": \"22841751e9b344e391a96b5dba8304a7\"\n       
    },\n        {\n            \"m_Id\": \"1c4270d1dc214e1e905126be7f2ca0a4\"\n       
    }\n    ],\n    \"synonyms\": [\n        \"division\",\n        \"divided by\"\n   
    ],\n    \"m_Precision\": 0,\n    \"m_PreviewExpanded\": false,\n    \"m_DismissedVersion\":
    0,\n    \"m_PreviewMode\": 0,\n    \"m_CustomColors\": {\n        \"m_SerializableColors\":
    []\n    }\n}\n\n{\n    \"m_SGVersion\": 0,\n    \"m_Type\": \"UnityEditor.ShaderGraph.Vector3MaterialSlot\",\n   
    \"m_ObjectId\": \"99bbae58e0c047869ecf7b3caf8a26fd\",\n    \"m_Id\": 1,\n   
    \"m_DisplayName\": \"Out\",\n    \"m_SlotType\": 1,\n    \"m_Hidden\": false,\n   
    \"m_ShaderOutputName\": \"Out\",\n    \"m_StageCapability\": 3,\n    \"m_Value\":
    {\n        \"x\": 0.0,\n        \"y\": 0.0,\n        \"z\": 0.0\n    },\n   
    \"m_DefaultValue\": {\n        \"x\": 0.0,\n        \"y\": 0.0,\n        \"z\":
    0.0\n    },\n    \"m_Labels\": []\n}\n\n{\n    \"m_SGVersion\": 0,\n    \"m_Type\":
    \"UnityEditor.ShaderGraph.DynamicVectorMaterialSlot\",\n    \"m_ObjectId\": \"9a35b78783884424b2968e695112d16e\",\n   
    \"m_Id\": 0,\n    \"m_DisplayName\": \"In\",\n    \"m_SlotType\": 0,\n    \"m_Hidden\":
    false,\n    \"m_ShaderOutputName\": \"In\",\n    \"m_StageCapability\": 3,\n   
    \"m_Value\": {\n        \"x\": 0.0,\n        \"y\": 0.0,\n        \"z\": 0.0,\n       
    \"w\": 0.0\n    },\n    \"m_DefaultValue\": {\n        \"x\": 0.0,\n        \"y\":
    0.0,\n        \"z\": 0.0,\n        \"w\": 0.0\n    }\n}\n\n{\n    \"m_SGVersion\":
    0,\n    \"m_Type\": \"UnityEditor.ShaderGraph.DynamicVectorMaterialSlot\",\n   
    \"m_ObjectId\": \"9f9787b3946c471da23b98dcc800bc1b\",\n    \"m_Id\": 0,\n   
    \"m_DisplayName\": \"A\",\n    \"m_SlotType\": 0,\n    \"m_Hidden\": false,\n   
    \"m_ShaderOutputName\": \"A\",\n    \"m_StageCapability\": 3,\n    \"m_Value\":
    {\n        \"x\": 1.0,\n        \"y\": 1.0,\n        \"z\": 1.0,\n        \"w\":
    1.0\n    },\n    \"m_DefaultValue\": {\n        \"x\": 0.0,\n        \"y\": 0.0,\n       
    \"z\": 0.0,\n        \"w\": 0.0\n    }\n}\n\n{\n    \"m_SGVersion\": 0,\n   
    \"m_Type\": \"UnityEditor.ShaderGraph.ScreenPositionNode\",\n    \"m_ObjectId\":
    \"a7b7335a11aa40aa97b94bde0c88c687\",\n    \"m_Group\": {\n        \"m_Id\":
    \"76209a890b4c4c25bb877c5cca04c8bf\"\n    },\n    \"m_Name\": \"Screen Position\",\n   
    \"m_DrawState\": {\n        \"m_Expanded\": true,\n        \"m_Position\": {\n           
    \"serializedVersion\": \"2\",\n            \"x\": -2430.0,\n            \"y\":
    376.0,\n            \"width\": 145.0,\n            \"height\": 129.00006103515626\n       
    }\n    },\n    \"m_Slots\": [\n        {\n            \"m_Id\": \"ac0da66da6ad48729b5f034874c9ee28\"\n       
    }\n    ],\n    \"synonyms\": [],\n    \"m_Precision\": 0,\n    \"m_PreviewExpanded\":
    false,\n    \"m_DismissedVersion\": 0,\n    \"m_PreviewMode\": 0,\n    \"m_CustomColors\":
    {\n        \"m_SerializableColors\": []\n    },\n    \"m_ScreenSpaceType\": 1\n}\n\n{\n   
    \"m_SGVersion\": 0,\n    \"m_Type\": \"UnityEditor.ShaderGraph.Vector4MaterialSlot\",\n   
    \"m_ObjectId\": \"ac0da66da6ad48729b5f034874c9ee28\",\n    \"m_Id\": 0,\n   
    \"m_DisplayName\": \"Out\",\n    \"m_SlotType\": 1,\n    \"m_Hidden\": false,\n   
    \"m_ShaderOutputName\": \"Out\",\n    \"m_StageCapability\": 3,\n    \"m_Value\":
    {\n        \"x\": 0.0,\n        \"y\": 0.0,\n        \"z\": 0.0,\n        \"w\":
    0.0\n    },\n    \"m_DefaultValue\": {\n        \"x\": 0.0,\n        \"y\": 0.0,\n       
    \"z\": 0.0,\n        \"w\": 0.0\n    },\n    \"m_Labels\": []\n}\n\n{\n    \"m_SGVersion\":
    0,\n    \"m_Type\": \"UnityEditor.ShaderGraph.Vector1MaterialSlot\",\n    \"m_ObjectId\":
    \"ae0df2a7f6f74060a739411e445e4a0e\",\n    \"m_Id\": 4,\n    \"m_DisplayName\":
    \"A\",\n    \"m_SlotType\": 1,\n    \"m_Hidden\": false,\n    \"m_ShaderOutputName\":
    \"A\",\n    \"m_StageCapability\": 3,\n    \"m_Value\": 0.0,\n    \"m_DefaultValue\":
    0.0,\n    \"m_Labels\": []\n}\n\n{\n    \"m_SGVersion\": 0,\n    \"m_Type\":
    \"UnityEditor.ShaderGraph.NegateNode\",\n    \"m_ObjectId\": \"af2374d6328e475abe6e5841029b3445\",\n   
    \"m_Group\": {\n        \"m_Id\": \"979cab19952a45dcb36d4e7e8df32db9\"\n    },\n   
    \"m_Name\": \"Negate\",\n    \"m_DrawState\": {\n        \"m_Expanded\": true,\n       
    \"m_Position\": {\n            \"serializedVersion\": \"2\",\n            \"x\":
    -750.999755859375,\n            \"y\": 299.9999694824219,\n            \"width\":
    127.99993896484375,\n            \"height\": 94.0\n        }\n    },\n    \"m_Slots\":
    [\n        {\n            \"m_Id\": \"9a35b78783884424b2968e695112d16e\"\n       
    },\n        {\n            \"m_Id\": \"41fd6f88d86d48ec9e1492ed8452d2ee\"\n       
    }\n    ],\n    \"synonyms\": [\n        \"invert\",\n        \"opposite\"\n   
    ],\n    \"m_Precision\": 0,\n    \"m_PreviewExpanded\": false,\n    \"m_DismissedVersion\":
    0,\n    \"m_PreviewMode\": 0,\n    \"m_CustomColors\": {\n        \"m_SerializableColors\":
    []\n    }\n}\n\n{\n    \"m_SGVersion\": 0,\n    \"m_Type\": \"UnityEditor.ShaderGraph.ViewVectorNode\",\n   
    \"m_ObjectId\": \"afc85f2148d74310b95d90787bae30a5\",\n    \"m_Group\": {\n       
    \"m_Id\": \"8cbea1a8c24b48049fc7c273d337fda1\"\n    },\n    \"m_Name\": \"View
    Vector\",\n    \"m_DrawState\": {\n        \"m_Expanded\": true,\n        \"m_Position\":
    {\n            \"serializedVersion\": \"2\",\n            \"x\": -2055.0,\n           
    \"y\": 311.0,\n            \"width\": 206.0,\n            \"height\": 131.0\n       
    }\n    },\n    \"m_Slots\": [\n        {\n            \"m_Id\": \"fda8da5552ac48dc9c72ee6701030a54\"\n       
    },\n        {\n            \"m_Id\": \"29a2759c37e942b19b96816dedd63d46\"\n       
    }\n    ],\n    \"synonyms\": [\n        \"eye vector\"\n    ],\n    \"m_Precision\":
    0,\n    \"m_PreviewExpanded\": false,\n    \"m_DismissedVersion\": 0,\n    \"m_PreviewMode\":
    0,\n    \"m_CustomColors\": {\n        \"m_SerializableColors\": []\n    },\n   
    \"m_Space\": 2\n}\n\n{\n    \"m_SGVersion\": 0,\n    \"m_Type\": \"UnityEditor.ShaderGraph.DynamicVectorMaterialSlot\",\n   
    \"m_ObjectId\": \"b688ce36e65d40f78539a118af04d50d\",\n    \"m_Id\": 0,\n   
    \"m_DisplayName\": \"In\",\n    \"m_SlotType\": 0,\n    \"m_Hidden\": false,\n   
    \"m_ShaderOutputName\": \"In\",\n    \"m_StageCapability\": 3,\n    \"m_Value\":
    {\n        \"x\": 0.0,\n        \"y\": 0.0,\n        \"z\": 0.0,\n        \"w\":
    0.0\n    },\n    \"m_DefaultValue\": {\n        \"x\": 0.0,\n        \"y\": 0.0,\n       
    \"z\": 0.0,\n        \"w\": 0.0\n    }\n}\n\n{\n    \"m_SGVersion\": 0,\n   
    \"m_Type\": \"UnityEditor.ShaderGraph.DynamicVectorMaterialSlot\",\n    \"m_ObjectId\":
    \"b9f82f1cb96b4b789df2917ea886abab\",\n    \"m_Id\": 1,\n    \"m_DisplayName\":
    \"Out\",\n    \"m_SlotType\": 1,\n    \"m_Hidden\": false,\n    \"m_ShaderOutputName\":
    \"Out\",\n    \"m_StageCapability\": 3,\n    \"m_Value\": {\n        \"x\": 0.0,\n       
    \"y\": 0.0,\n        \"z\": 0.0,\n        \"w\": 0.0\n    },\n    \"m_DefaultValue\":
    {\n        \"x\": 0.0,\n        \"y\": 0.0,\n        \"z\": 0.0,\n        \"w\":
    0.0\n    }\n}\n\n{\n    \"m_SGVersion\": 0,\n    \"m_Type\": \"UnityEditor.ShaderGraph.BlockNode\",\n   
    \"m_ObjectId\": \"baac8694b57f43ae858c4063999f86d5\",\n    \"m_Group\": {\n       
    \"m_Id\": \"\"\n    },\n    \"m_Name\": \"SurfaceDescription.BaseColor\",\n   
    \"m_DrawState\": {\n        \"m_Expanded\": true,\n        \"m_Position\": {\n           
    \"serializedVersion\": \"2\",\n            \"x\": 11.000069618225098,\n           
    \"y\": 234.00001525878907,\n            \"width\": 200.0,\n            \"height\":
    40.99998474121094\n        }\n    },\n    \"m_Slots\": [\n        {\n           
    \"m_Id\": \"f8e7f879239c48bc85a0fdea5951c067\"\n        }\n    ],\n    \"synonyms\":
    [],\n    \"m_Precision\": 0,\n    \"m_PreviewExpanded\": true,\n    \"m_DismissedVersion\":
    0,\n    \"m_PreviewMode\": 0,\n    \"m_CustomColors\": {\n        \"m_SerializableColors\":
    []\n    },\n    \"m_SerializedDescriptor\": \"SurfaceDescription.BaseColor\"\n}\n\n{\n   
    \"m_SGVersion\": 0,\n    \"m_Type\": \"UnityEditor.ShaderGraph.Vector1MaterialSlot\",\n   
    \"m_ObjectId\": \"c13dd4368c9545c3b404ec5215cf4ff1\",\n    \"m_Id\": 2,\n   
    \"m_DisplayName\": \"G\",\n    \"m_SlotType\": 1,\n    \"m_Hidden\": false,\n   
    \"m_ShaderOutputName\": \"G\",\n    \"m_StageCapability\": 3,\n    \"m_Value\":
    0.0,\n    \"m_DefaultValue\": 0.0,\n    \"m_Labels\": []\n}\n\n{\n    \"m_SGVersion\":
    0,\n    \"m_Type\": \"UnityEditor.ShaderGraph.Vector1MaterialSlot\",\n    \"m_ObjectId\":
    \"cbc22be1e5fe47358368936e163178a2\",\n    \"m_Id\": 0,\n    \"m_DisplayName\":
    \"Alpha\",\n    \"m_SlotType\": 0,\n    \"m_Hidden\": false,\n    \"m_ShaderOutputName\":
    \"Alpha\",\n    \"m_StageCapability\": 2,\n    \"m_Value\": 1.0,\n    \"m_DefaultValue\":
    1.0,\n    \"m_Labels\": []\n}\n\n{\n    \"m_SGVersion\": 0,\n    \"m_Type\":
    \"UnityEditor.ShaderGraph.GroupData\",\n    \"m_ObjectId\": \"ce0298f98f104f44af2de9853c03d209\",\n   
    \"m_Title\": \"Scene Depth\",\n    \"m_Position\": {\n        \"x\": -2286.999755859375,\n       
    \"y\": 112.0\n    }\n}\n\n{\n    \"m_SGVersion\": 2,\n    \"m_Type\": \"UnityEditor.Rendering.Universal.ShaderGraph.UniversalUnlitSubTarget\",\n   
    \"m_ObjectId\": \"d38644b2fc92466594878722b29c366d\"\n}\n\n{\n    \"m_SGVersion\":
    0,\n    \"m_Type\": \"UnityEditor.ShaderGraph.Vector1MaterialSlot\",\n    \"m_ObjectId\":
    \"dd37a4879bc148c79a3b2fd503528185\",\n    \"m_Id\": 1,\n    \"m_DisplayName\":
    \"Out\",\n    \"m_SlotType\": 1,\n    \"m_Hidden\": false,\n    \"m_ShaderOutputName\":
    \"Out\",\n    \"m_StageCapability\": 2,\n    \"m_Value\": 0.0,\n    \"m_DefaultValue\":
    0.0,\n    \"m_Labels\": []\n}\n\n{\n    \"m_SGVersion\": 1,\n    \"m_Type\":
    \"UnityEditor.Rendering.Universal.ShaderGraph.UniversalTarget\",\n    \"m_ObjectId\":
    \"e257204e327d413bb74d15d9de1aaa3d\",\n    \"m_Datas\": [],\n    \"m_ActiveSubTarget\":
    {\n        \"m_Id\": \"d38644b2fc92466594878722b29c366d\"\n    },\n    \"m_AllowMaterialOverride\":
    false,\n    \"m_SurfaceType\": 1,\n    \"m_ZTestMode\": 4,\n    \"m_ZWriteControl\":
    0,\n    \"m_AlphaMode\": 0,\n    \"m_RenderFace\": 2,\n    \"m_AlphaClip\": false,\n   
    \"m_CastShadows\": true,\n    \"m_ReceiveShadows\": true,\n    \"m_SupportsLODCrossFade\":
    false,\n    \"m_CustomEditorGUI\": \"\",\n    \"m_SupportVFX\": false\n}\n\n{\n   
    \"m_SGVersion\": 0,\n    \"m_Type\": \"UnityEditor.ShaderGraph.MultiplyNode\",\n   
    \"m_ObjectId\": \"e5f2b1b42c764f6fb9ea38eeb72a1764\",\n    \"m_Group\": {\n       
    \"m_Id\": \"\"\n    },\n    \"m_Name\": \"Multiply\",\n    \"m_DrawState\": {\n       
    \"m_Expanded\": true,\n        \"m_Position\": {\n            \"serializedVersion\":
    \"2\",\n            \"x\": -148.99998474121095,\n            \"y\": 275.9999694824219,\n           
    \"width\": 207.99990844726563,\n            \"height\": 302.0000305175781\n       
    }\n    },\n    \"m_Slots\": [\n        {\n            \"m_Id\": \"966bdaf3116e410a8ca3110dcfa37dfe\"\n       
    },\n        {\n            \"m_Id\": \"484ca351baee4e63aacef3966d1d8c06\"\n       
    },\n        {\n            \"m_Id\": \"43db111a153b48099132feee3ee16142\"\n       
    }\n    ],\n    \"synonyms\": [\n        \"multiplication\",\n        \"times\",\n       
    \"x\"\n    ],\n    \"m_Precision\": 0,\n    \"m_PreviewExpanded\": false,\n   
    \"m_DismissedVersion\": 0,\n    \"m_PreviewMode\": 0,\n    \"m_CustomColors\":
    {\n        \"m_SerializableColors\": []\n    }\n}\n\n{\n    \"m_SGVersion\":
    2,\n    \"m_Type\": \"UnityEditor.ShaderGraph.TransformNode\",\n    \"m_ObjectId\":
    \"e66a87ef4d48494d9052e474b737aa46\",\n    \"m_Group\": {\n        \"m_Id\":
    \"979cab19952a45dcb36d4e7e8df32db9\"\n    },\n    \"m_Name\": \"Transform\",\n   
    \"m_DrawState\": {\n        \"m_Expanded\": true,\n        \"m_Position\": {\n           
    \"serializedVersion\": \"2\",\n            \"x\": -1149.999755859375,\n           
    \"y\": 182.99996948242188,\n            \"width\": 212.9998779296875,\n           
    \"height\": 157.00003051757813\n        }\n    },\n    \"m_Slots\": [\n       
    {\n            \"m_Id\": \"fe600b4ef2c14e9680474811f66fd741\"\n        },\n       
    {\n            \"m_Id\": \"99bbae58e0c047869ecf7b3caf8a26fd\"\n        }\n   
    ],\n    \"synonyms\": [\n        \"world\",\n        \"tangent\",\n        \"object\",\n       
    \"view\",\n        \"screen\",\n        \"convert\"\n    ],\n    \"m_Precision\":
    0,\n    \"m_PreviewExpanded\": false,\n    \"m_DismissedVersion\": 0,\n    \"m_PreviewMode\":
    0,\n    \"m_CustomColors\": {\n        \"m_SerializableColors\": []\n    },\n   
    \"m_Conversion\": {\n        \"from\": 2,\n        \"to\": 0\n    },\n    \"m_ConversionType\":
    0,\n    \"m_Normalize\": true\n}\n\n{\n    \"m_SGVersion\": 0,\n    \"m_Type\":
    \"UnityEditor.ShaderGraph.NormalMaterialSlot\",\n    \"m_ObjectId\": \"e712508558494429a5b81da9616cc5e2\",\n   
    \"m_Id\": 0,\n    \"m_DisplayName\": \"Normal\",\n    \"m_SlotType\": 0,\n   
    \"m_Hidden\": false,\n    \"m_ShaderOutputName\": \"Normal\",\n    \"m_StageCapability\":
    1,\n    \"m_Value\": {\n        \"x\": 0.0,\n        \"y\": 0.0,\n        \"z\":
    0.0\n    },\n    \"m_DefaultValue\": {\n        \"x\": 0.0,\n        \"y\": 0.0,\n       
    \"z\": 0.0\n    },\n    \"m_Labels\": [],\n    \"m_Space\": 0\n}\n\n{\n    \"m_SGVersion\":
    0,\n    \"m_Type\": \"UnityEditor.ShaderGraph.PositionMaterialSlot\",\n    \"m_ObjectId\":
    \"e8f58791e8e343899c681682fd1030d7\",\n    \"m_Id\": 0,\n    \"m_DisplayName\":
    \"Position\",\n    \"m_SlotType\": 0,\n    \"m_Hidden\": false,\n    \"m_ShaderOutputName\":
    \"Position\",\n    \"m_StageCapability\": 1,\n    \"m_Value\": {\n        \"x\":
    0.0,\n        \"y\": 0.0,\n        \"z\": 0.0\n    },\n    \"m_DefaultValue\":
    {\n        \"x\": 0.0,\n        \"y\": 0.0,\n        \"z\": 0.0\n    },\n   
    \"m_Labels\": [],\n    \"m_Space\": 0\n}\n\n{\n    \"m_SGVersion\": 0,\n    \"m_Type\":
    \"UnityEditor.ShaderGraph.Vector1MaterialSlot\",\n    \"m_ObjectId\": \"f239c58829eb47d99b69cd3030caadc8\",\n   
    \"m_Id\": 3,\n    \"m_DisplayName\": \"B\",\n    \"m_SlotType\": 1,\n    \"m_Hidden\":
    false,\n    \"m_ShaderOutputName\": \"B\",\n    \"m_StageCapability\": 3,\n   
    \"m_Value\": 0.0,\n    \"m_DefaultValue\": 0.0,\n    \"m_Labels\": []\n}\n\n{\n   
    \"m_SGVersion\": 0,\n    \"m_Type\": \"UnityEditor.ShaderGraph.Vector4MaterialSlot\",\n   
    \"m_ObjectId\": \"f2d0b4518d69453cae8127d457fa12e7\",\n    \"m_Id\": 0,\n   
    \"m_DisplayName\": \"Color\",\n    \"m_SlotType\": 1,\n    \"m_Hidden\": false,\n   
    \"m_ShaderOutputName\": \"Out\",\n    \"m_StageCapability\": 3,\n    \"m_Value\":
    {\n        \"x\": 0.0,\n        \"y\": 0.0,\n        \"z\": 0.0,\n        \"w\":
    0.0\n    },\n    \"m_DefaultValue\": {\n        \"x\": 0.0,\n        \"y\": 0.0,\n       
    \"z\": 0.0,\n        \"w\": 0.0\n    },\n    \"m_Labels\": []\n}\n\n{\n    \"m_SGVersion\":
    0,\n    \"m_Type\": \"UnityEditor.ShaderGraph.SplitNode\",\n    \"m_ObjectId\":
    \"f5b088c7833b4f9eb622fb2970975e0e\",\n    \"m_Group\": {\n        \"m_Id\":
    \"\"\n    },\n    \"m_Name\": \"Split\",\n    \"m_DrawState\": {\n        \"m_Expanded\":
    false,\n        \"m_Position\": {\n            \"serializedVersion\": \"2\",\n           
    \"x\": -294.99981689453127,\n            \"y\": 275.99993896484377,\n           
    \"width\": 119.00006103515625,\n            \"height\": 77.00009155273438\n       
    }\n    },\n    \"m_Slots\": [\n        {\n            \"m_Id\": \"7c05a238379c429aa44d044507009474\"\n       
    },\n        {\n            \"m_Id\": \"6f9df22cbaef4542aeff74ed0045c7f0\"\n       
    },\n        {\n            \"m_Id\": \"0e1554df7f1c4c2db42f8b6ca2819702\"\n       
    },\n        {\n            \"m_Id\": \"f239c58829eb47d99b69cd3030caadc8\"\n       
    },\n        {\n            \"m_Id\": \"ae0df2a7f6f74060a739411e445e4a0e\"\n       
    }\n    ],\n    \"synonyms\": [\n        \"separate\"\n    ],\n    \"m_Precision\":
    0,\n    \"m_PreviewExpanded\": true,\n    \"m_DismissedVersion\": 0,\n    \"m_PreviewMode\":
    0,\n    \"m_CustomColors\": {\n        \"m_SerializableColors\": []\n    }\n}\n\n{\n   
    \"m_SGVersion\": 0,\n    \"m_Type\": \"UnityEditor.ShaderGraph.DynamicVectorMaterialSlot\",\n   
    \"m_ObjectId\": \"f6de9e191c0945b5bdf50e51e6b1966a\",\n    \"m_Id\": 0,\n   
    \"m_DisplayName\": \"In\",\n    \"m_SlotType\": 0,\n    \"m_Hidden\": false,\n   
    \"m_ShaderOutputName\": \"In\",\n    \"m_StageCapability\": 3,\n    \"m_Value\":
    {\n        \"x\": 0.0,\n        \"y\": 0.0,\n        \"z\": 0.0,\n        \"w\":
    0.0\n    },\n    \"m_DefaultValue\": {\n        \"x\": 0.0,\n        \"y\": 0.0,\n       
    \"z\": 0.0,\n        \"w\": 0.0\n    }\n}\n\n{\n    \"m_SGVersion\": 0,\n   
    \"m_Type\": \"UnityEditor.ShaderGraph.MultiplyNode\",\n    \"m_ObjectId\": \"f867acc8aa524466baaf93a23b8fe22d\",\n   
    \"m_Group\": {\n        \"m_Id\": \"8cbea1a8c24b48049fc7c273d337fda1\"\n    },\n   
    \"m_Name\": \"Multiply\",\n    \"m_DrawState\": {\n        \"m_Expanded\": true,\n       
    \"m_Position\": {\n            \"serializedVersion\": \"2\",\n            \"x\":
    -1653.0,\n            \"y\": 214.0,\n            \"width\": 130.0,\n           
    \"height\": 118.00003051757813\n        }\n    },\n    \"m_Slots\": [\n       
    {\n            \"m_Id\": \"5d79c6b0be7a4918a5d10c6d9814e210\"\n        },\n       
    {\n            \"m_Id\": \"7d2b94fa53cb48be93caf642f0bd532f\"\n        },\n       
    {\n            \"m_Id\": \"2333e30594da400baa82dece2c340147\"\n        }\n   
    ],\n    \"synonyms\": [\n        \"multiplication\",\n        \"times\",\n       
    \"x\"\n    ],\n    \"m_Precision\": 0,\n    \"m_PreviewExpanded\": false,\n   
    \"m_DismissedVersion\": 0,\n    \"m_PreviewMode\": 0,\n    \"m_CustomColors\":
    {\n        \"m_SerializableColors\": []\n    }\n}\n\n{\n    \"m_SGVersion\":
    0,\n    \"m_Type\": \"UnityEditor.ShaderGraph.ColorRGBMaterialSlot\",\n    \"m_ObjectId\":
    \"f8e7f879239c48bc85a0fdea5951c067\",\n    \"m_Id\": 0,\n    \"m_DisplayName\":
    \"Base Color\",\n    \"m_SlotType\": 0,\n    \"m_Hidden\": false,\n    \"m_ShaderOutputName\":
    \"BaseColor\",\n    \"m_StageCapability\": 2,\n    \"m_Value\": {\n        \"x\":
    0.5,\n        \"y\": 0.5,\n        \"z\": 0.5\n    },\n    \"m_DefaultValue\":
    {\n        \"x\": 0.0,\n        \"y\": 0.0,\n        \"z\": 0.0\n    },\n   
    \"m_Labels\": [],\n    \"m_ColorMode\": 0,\n    \"m_DefaultColor\": {\n       
    \"r\": 0.5,\n        \"g\": 0.5,\n        \"b\": 0.5,\n        \"a\": 1.0\n   
    }\n}\n\n{\n    \"m_SGVersion\": 0,\n    \"m_Type\": \"UnityEditor.ShaderGraph.DynamicVectorMaterialSlot\",\n   
    \"m_ObjectId\": \"fd24c513279946d094961ef948751029\",\n    \"m_Id\": 0,\n   
    \"m_DisplayName\": \"A\",\n    \"m_SlotType\": 0,\n    \"m_Hidden\": false,\n   
    \"m_ShaderOutputName\": \"A\",\n    \"m_StageCapability\": 3,\n    \"m_Value\":
    {\n        \"x\": 0.0,\n        \"y\": 0.0,\n        \"z\": 0.0,\n        \"w\":
    0.0\n    },\n    \"m_DefaultValue\": {\n        \"x\": 0.0,\n        \"y\": 0.0,\n       
    \"z\": 0.0,\n        \"w\": 0.0\n    }\n}\n\n{\n    \"m_SGVersion\": 0,\n   
    \"m_Type\": \"UnityEditor.ShaderGraph.PositionMaterialSlot\",\n    \"m_ObjectId\":
    \"fda8da5552ac48dc9c72ee6701030a54\",\n    \"m_Id\": 3,\n    \"m_DisplayName\":
    \"World Space Position\",\n    \"m_SlotType\": 0,\n    \"m_Hidden\": true,\n   
    \"m_ShaderOutputName\": \"WorldSpacePosition\",\n    \"m_StageCapability\": 3,\n   
    \"m_Value\": {\n        \"x\": 0.0,\n        \"y\": 0.0,\n        \"z\": 0.0\n   
    },\n    \"m_DefaultValue\": {\n        \"x\": 0.0,\n        \"y\": 0.0,\n       
    \"z\": 0.0\n    },\n    \"m_Labels\": [],\n    \"m_Space\": 2\n}\n\n{\n    \"m_SGVersion\":
    0,\n    \"m_Type\": \"UnityEditor.ShaderGraph.Vector3MaterialSlot\",\n    \"m_ObjectId\":
    \"fe600b4ef2c14e9680474811f66fd741\",\n    \"m_Id\": 0,\n    \"m_DisplayName\":
    \"In\",\n    \"m_SlotType\": 0,\n    \"m_Hidden\": false,\n    \"m_ShaderOutputName\":
    \"In\",\n    \"m_StageCapability\": 3,\n    \"m_Value\": {\n        \"x\": 0.0,\n       
    \"y\": 0.0,\n        \"z\": 0.0\n    },\n    \"m_DefaultValue\": {\n        \"x\":
    0.0,\n        \"y\": 0.0,\n        \"z\": 0.0\n    },\n    \"m_Labels\": []\n}\n\n"
  m_AssetMaybeChangedOnDisk: 0
  m_AssetMaybeDeleted: 0
--- !u!114 &16
MonoBehaviour:
  m_ObjectHideFlags: 52
  m_CorrespondingSourceObject: {fileID: 0}
  m_PrefabInstance: {fileID: 0}
  m_PrefabAsset: {fileID: 0}
  m_GameObject: {fileID: 0}
  m_Enabled: 1
  m_EditorHideFlags: 0
  m_Script: {fileID: 13854, guid: 0000000000000000e000000000000000, type: 0}
  m_Name: 
  m_EditorClassIdentifier: 
  m_MinSize: {x: 310, y: 200}
  m_MaxSize: {x: 4000, y: 4000}
  m_TitleContent:
    m_Text: Project Settings
    m_Image: {fileID: -5712115415447495865, guid: 0000000000000000d000000000000000,
      type: 0}
    m_Tooltip: 
  m_Pos:
    serializedVersion: 2
    x: 0
    y: 73
    width: 1920
    height: 918
  m_SerializedDataModeController:
    m_DataMode: 0
    m_PreferredDataMode: 0
    m_SupportedDataModes: 
    isAutomatic: 1
  m_ViewDataDictionary: {fileID: 0}
  m_OverlayCanvas:
    m_LastAppliedPresetName: Default
    m_SaveData: []
    m_OverlaysVisible: 1
  m_PosLeft: {x: 0, y: 0}
  m_PosRight: {x: 0, y: 0}
  m_Scope: 1
  m_SplitterFlex: 0.11736527
  m_SearchText: 
  m_TreeViewState:
    scrollPos: {x: 0, y: 0}
    m_SelectedIDs: 4dcf9b58
    m_LastClickedID: 1486606157
    m_ExpandedIDs: 53336be200000000
    m_RenameOverlay:
      m_UserAcceptedRename: 0
      m_Name: 
      m_OriginalName: 
      m_EditFieldRect:
        serializedVersion: 2
        x: 0
        y: 0
        width: 0
        height: 0
      m_UserData: 0
      m_IsWaitingForDelay: 0
      m_IsRenaming: 0
      m_OriginalEventType: 11
      m_IsRenamingFilename: 0
      m_ClientGUIView: {fileID: 0}
    m_SearchString: 
--- !u!114 &17
MonoBehaviour:
  m_ObjectHideFlags: 52
  m_CorrespondingSourceObject: {fileID: 0}
  m_PrefabInstance: {fileID: 0}
  m_PrefabAsset: {fileID: 0}
  m_GameObject: {fileID: 0}
  m_Enabled: 1
  m_EditorHideFlags: 0
  m_Script: {fileID: 12003, guid: 0000000000000000e000000000000000, type: 0}
  m_Name: 
  m_EditorClassIdentifier: 
  m_MinSize: {x: 100, y: 100}
  m_MaxSize: {x: 4000, y: 4000}
  m_TitleContent:
    m_Text: Console
    m_Image: {fileID: -4327648978806127646, guid: 0000000000000000d000000000000000,
      type: 0}
    m_Tooltip: 
  m_Pos:
    serializedVersion: 2
    x: 2904
    y: 672
    width: 694
    height: 267
  m_SerializedDataModeController:
    m_DataMode: 0
    m_PreferredDataMode: 0
    m_SupportedDataModes: 
    isAutomatic: 1
  m_ViewDataDictionary: {fileID: 0}
  m_OverlayCanvas:
    m_LastAppliedPresetName: Default
    m_SaveData: []
    m_OverlaysVisible: 1
--- !u!114 &18
MonoBehaviour:
  m_ObjectHideFlags: 52
  m_CorrespondingSourceObject: {fileID: 0}
  m_PrefabInstance: {fileID: 0}
  m_PrefabAsset: {fileID: 0}
  m_GameObject: {fileID: 0}
  m_Enabled: 1
  m_EditorHideFlags: 1
  m_Script: {fileID: 12061, guid: 0000000000000000e000000000000000, type: 0}
  m_Name: 
  m_EditorClassIdentifier: 
  m_MinSize: {x: 200, y: 200}
  m_MaxSize: {x: 4000, y: 4000}
  m_TitleContent:
    m_Text: Hierarchy
    m_Image: {fileID: 7966133145522015247, guid: 0000000000000000d000000000000000,
      type: 0}
    m_Tooltip: 
  m_Pos:
    serializedVersion: 2
    x: 1056
    y: 125
    width: 291
    height: 546
  m_SerializedDataModeController:
    m_DataMode: 0
    m_PreferredDataMode: 0
    m_SupportedDataModes: 
    isAutomatic: 1
  m_ViewDataDictionary: {fileID: 0}
  m_OverlayCanvas:
    m_LastAppliedPresetName: Default
    m_SaveData: []
    m_OverlaysVisible: 1
  m_SceneHierarchy:
    m_TreeViewState:
      scrollPos: {x: 0, y: 0}
      m_SelectedIDs: 0c6c0000
      m_LastClickedID: 0
      m_ExpandedIDs: 389cf9ff069ef9ffe49ef9ff2874faff8c86fbff8e86fbfffafaffff
      m_RenameOverlay:
        m_UserAcceptedRename: 0
        m_Name: 
        m_OriginalName: 
        m_EditFieldRect:
          serializedVersion: 2
          x: 0
          y: 0
          width: 0
          height: 0
        m_UserData: 0
        m_IsWaitingForDelay: 0
        m_IsRenaming: 0
        m_OriginalEventType: 11
        m_IsRenamingFilename: 0
        m_ClientGUIView: {fileID: 8}
      m_SearchString: 
    m_ExpandedScenes: []
    m_CurrenRootInstanceID: 0
    m_LockTracker:
      m_IsLocked: 0
    m_CurrentSortingName: TransformSorting
  m_WindowGUID: 304babaaa69ca564481b0de7a0a6038a
--- !u!114 &19
MonoBehaviour:
  m_ObjectHideFlags: 52
  m_CorrespondingSourceObject: {fileID: 0}
  m_PrefabInstance: {fileID: 0}
  m_PrefabAsset: {fileID: 0}
  m_GameObject: {fileID: 0}
  m_Enabled: 1
  m_EditorHideFlags: 1
  m_Script: {fileID: 12019, guid: 0000000000000000e000000000000000, type: 0}
  m_Name: 
  m_EditorClassIdentifier: 
  m_MinSize: {x: 275, y: 50}
  m_MaxSize: {x: 4000, y: 4000}
  m_TitleContent:
    m_Text: Inspector
    m_Image: {fileID: -2667387946076563598, guid: 0000000000000000d000000000000000,
      type: 0}
    m_Tooltip: 
  m_Pos:
    serializedVersion: 2
    x: 1348
    y: 125
    width: 401
    height: 546
  m_SerializedDataModeController:
    m_DataMode: 0
    m_PreferredDataMode: 0
    m_SupportedDataModes: 
    isAutomatic: 1
  m_ViewDataDictionary: {fileID: 0}
  m_OverlayCanvas:
    m_LastAppliedPresetName: Default
    m_SaveData: []
    m_OverlaysVisible: 1
  m_ObjectsLockedBeforeSerialization: []
  m_InstanceIDsLockedBeforeSerialization: 
  m_PreviewResizer:
    m_CachedPref: 160
    m_ControlHash: -371814159
    m_PrefName: Preview_InspectorPreview
  m_LastInspectedObjectInstanceID: -1
  m_LastVerticalScrollValue: 0
  m_GlobalObjectId: 
  m_InspectorMode: 0
  m_LockTracker:
    m_IsLocked: 0
  m_PreviewWindow: {fileID: 0}
--- !u!114 &20
MonoBehaviour:
  m_ObjectHideFlags: 52
  m_CorrespondingSourceObject: {fileID: 0}
  m_PrefabInstance: {fileID: 0}
  m_PrefabAsset: {fileID: 0}
  m_GameObject: {fileID: 0}
  m_Enabled: 1
  m_EditorHideFlags: 1
  m_Script: {fileID: 12014, guid: 0000000000000000e000000000000000, type: 0}
  m_Name: 
  m_EditorClassIdentifier: 
  m_MinSize: {x: 230, y: 250}
  m_MaxSize: {x: 10000, y: 10000}
  m_TitleContent:
    m_Text: Project
    m_Image: {fileID: -5467254957812901981, guid: 0000000000000000d000000000000000,
      type: 0}
    m_Tooltip: 
  m_Pos:
    serializedVersion: 2
    x: 1056
    y: 694
    width: 694
    height: 267
  m_SerializedDataModeController:
    m_DataMode: 0
    m_PreferredDataMode: 0
    m_SupportedDataModes: 
    isAutomatic: 1
  m_ViewDataDictionary: {fileID: 0}
  m_OverlayCanvas:
    m_LastAppliedPresetName: Default
    m_SaveData: []
    m_OverlaysVisible: 1
  m_SearchFilter:
    m_NameFilter: 
    m_ClassNames: []
    m_AssetLabels: []
    m_AssetBundleNames: []
    m_VersionControlStates: []
    m_SoftLockControlStates: []
    m_ReferencingInstanceIDs: 
    m_SceneHandles: 
    m_ShowAllHits: 0
    m_SkipHidden: 0
    m_SearchArea: 1
    m_Folders:
    - Assets/PackageExporter/unityignore
    m_Globs: []
    m_OriginalText: 
    m_ImportLogFlags: 0
  m_ViewMode: 1
  m_StartGridSize: 16
  m_LastFolders:
  - Assets/PackageExporter/unityignore
  m_LastFoldersGridSize: 16
  m_LastProjectPath: D:\_workspace\Unity\Particle
  m_LockTracker:
    m_IsLocked: 0
  m_FolderTreeState:
    scrollPos: {x: 0, y: 189}
    m_SelectedIDs: d06b0000
    m_LastClickedID: 27600
    m_ExpandedIDs: 00000000c6680000c8680000ca680000cc680000ce680000d2680000626a0000666a000000ca9a3b
    m_RenameOverlay:
      m_UserAcceptedRename: 0
      m_Name: Materials
      m_OriginalName: Materials
      m_EditFieldRect:
        serializedVersion: 2
        x: 0
        y: 0
        width: 0
        height: 0
      m_UserData: 27552
      m_IsWaitingForDelay: 0
      m_IsRenaming: 0
      m_OriginalEventType: 0
      m_IsRenamingFilename: 1
      m_ClientGUIView: {fileID: 6}
    m_SearchString: 
    m_CreateAssetUtility:
      m_EndAction: {fileID: 0}
      m_InstanceID: 0
      m_Path: 
      m_Icon: {fileID: 0}
      m_ResourceFile: 
  m_AssetTreeState:
    scrollPos: {x: 0, y: 0}
    m_SelectedIDs: 
    m_LastClickedID: 0
    m_ExpandedIDs: 00000000c6680000c8680000ca680000cc680000ce680000d0680000d2680000
    m_RenameOverlay:
      m_UserAcceptedRename: 0
      m_Name: 
      m_OriginalName: 
      m_EditFieldRect:
        serializedVersion: 2
        x: 0
        y: 0
        width: 0
        height: 0
      m_UserData: 0
      m_IsWaitingForDelay: 0
      m_IsRenaming: 0
      m_OriginalEventType: 11
      m_IsRenamingFilename: 1
      m_ClientGUIView: {fileID: 0}
    m_SearchString: 
    m_CreateAssetUtility:
      m_EndAction: {fileID: 0}
      m_InstanceID: 0
      m_Path: 
      m_Icon: {fileID: 0}
      m_ResourceFile: 
  m_ListAreaState:
    m_SelectedInstanceIDs: 0c6c0000
    m_LastClickedInstanceID: 27660
    m_HadKeyboardFocusLastEvent: 1
    m_ExpandedInstanceIDs: 00000000
    m_RenameOverlay:
      m_UserAcceptedRename: 0
      m_Name: PS_FallingLeaves
      m_OriginalName: PS_FallingLeaves
      m_EditFieldRect:
        serializedVersion: 2
        x: 0
        y: 0
        width: 0
        height: 0
      m_UserData: 27660
      m_IsWaitingForDelay: 0
      m_IsRenaming: 0
      m_OriginalEventType: 0
      m_IsRenamingFilename: 1
      m_ClientGUIView: {fileID: 6}
    m_CreateAssetUtility:
      m_EndAction: {fileID: 0}
      m_InstanceID: 0
      m_Path: 
      m_Icon: {fileID: 0}
      m_ResourceFile: 
    m_NewAssetIndexInList: -1
    m_ScrollPosition: {x: 0, y: 0}
    m_GridSize: 16
  m_SkipHiddenPackages: 0
  m_DirectoriesAreaWidth: 203
--- !u!114 &21
MonoBehaviour:
  m_ObjectHideFlags: 52
  m_CorrespondingSourceObject: {fileID: 0}
  m_PrefabInstance: {fileID: 0}
  m_PrefabAsset: {fileID: 0}
  m_GameObject: {fileID: 0}
  m_Enabled: 1
  m_EditorHideFlags: 1
  m_Script: {fileID: 12015, guid: 0000000000000000e000000000000000, type: 0}
  m_Name: 
  m_EditorClassIdentifier: 
  m_MinSize: {x: 200, y: 200}
  m_MaxSize: {x: 4000, y: 4000}
  m_TitleContent:
    m_Text: Game
    m_Image: {fileID: -6423792434712278376, guid: 0000000000000000d000000000000000,
      type: 0}
    m_Tooltip: 
  m_Pos:
    serializedVersion: 2
    x: 0
    y: 73
    width: 1920
    height: 918
  m_SerializedDataModeController:
    m_DataMode: 0
    m_PreferredDataMode: 0
    m_SupportedDataModes: 
    isAutomatic: 1
  m_ViewDataDictionary: {fileID: 0}
  m_OverlayCanvas:
    m_LastAppliedPresetName: Default
    m_SaveData: []
    m_OverlaysVisible: 1
  m_SerializedViewNames: []
  m_SerializedViewValues: []
  m_PlayModeViewName: GameView
  m_ShowGizmos: 0
  m_TargetDisplay: 0
  m_ClearColor: {r: 0, g: 0, b: 0, a: 0}
  m_TargetSize: {x: 1920, y: 897}
  m_TextureFilterMode: 0
  m_TextureHideFlags: 61
  m_RenderIMGUI: 1
  m_EnterPlayModeBehavior: 0
  m_UseMipMap: 0
  m_VSyncEnabled: 0
  m_Gizmos: 0
  m_Stats: 0
  m_SelectedSizes: 00000000000000000000000000000000000000000000000000000000000000000000000000000000
  m_ZoomArea:
    m_HRangeLocked: 0
    m_VRangeLocked: 0
    hZoomLockedByDefault: 0
    vZoomLockedByDefault: 0
    m_HBaseRangeMin: -960
    m_HBaseRangeMax: 960
    m_VBaseRangeMin: -448.5
    m_VBaseRangeMax: 448.5
    m_HAllowExceedBaseRangeMin: 1
    m_HAllowExceedBaseRangeMax: 1
    m_VAllowExceedBaseRangeMin: 1
    m_VAllowExceedBaseRangeMax: 1
    m_ScaleWithWindow: 0
    m_HSlider: 0
    m_VSlider: 0
    m_IgnoreScrollWheelUntilClicked: 0
    m_EnableMouseInput: 1
    m_EnableSliderZoomHorizontal: 0
    m_EnableSliderZoomVertical: 0
    m_UniformScale: 1
    m_UpDirection: 1
    m_DrawArea:
      serializedVersion: 2
      x: 0
      y: 21
      width: 1920
      height: 897
    m_Scale: {x: 1, y: 1}
    m_Translation: {x: 960, y: 448.5}
    m_MarginLeft: 0
    m_MarginRight: 0
    m_MarginTop: 0
    m_MarginBottom: 0
    m_LastShownAreaInsideMargins:
      serializedVersion: 2
      x: -960
      y: -448.5
      width: 1920
      height: 897
    m_MinimalGUI: 1
  m_defaultScale: 1
  m_LastWindowPixelSize: {x: 1920, y: 918}
  m_ClearInEditMode: 1
  m_NoCameraWarning: 1
  m_LowResolutionForAspectRatios: 01000000000000000000
  m_XRRenderMode: 0
  m_RenderTexture: {fileID: 0}
--- !u!114 &22
MonoBehaviour:
  m_ObjectHideFlags: 52
  m_CorrespondingSourceObject: {fileID: 0}
  m_PrefabInstance: {fileID: 0}
  m_PrefabAsset: {fileID: 0}
  m_GameObject: {fileID: 0}
  m_Enabled: 1
  m_EditorHideFlags: 1
  m_Script: {fileID: 12013, guid: 0000000000000000e000000000000000, type: 0}
  m_Name: 
  m_EditorClassIdentifier: 
  m_MinSize: {x: 200, y: 200}
  m_MaxSize: {x: 4000, y: 4000}
  m_TitleContent:
    m_Text: Scene
    m_Image: {fileID: 2593428753322112591, guid: 0000000000000000d000000000000000,
      type: 0}
    m_Tooltip: 
  m_Pos:
    serializedVersion: 2
    x: 0
    y: 73
    width: 1920
    height: 918
  m_SerializedDataModeController:
    m_DataMode: 0
    m_PreferredDataMode: 0
    m_SupportedDataModes: 
    isAutomatic: 1
  m_ViewDataDictionary: {fileID: 0}
  m_OverlayCanvas:
    m_LastAppliedPresetName: Default
    m_SaveData:
    - dockPosition: 0
      containerId: overlay-toolbar__top
      floating: 0
      collapsed: 0
      displayed: 1
      snapOffset: {x: -166, y: -26}
      snapOffsetDelta: {x: 0, y: 0}
      snapCorner: 3
      id: Tool Settings
      index: 0
      layout: 1
      size: {x: 0, y: 0}
      sizeOverriden: 0
    - dockPosition: 0
      containerId: overlay-toolbar__top
      floating: 0
      collapsed: 0
      displayed: 1
      snapOffset: {x: -141, y: 149}
      snapOffsetDelta: {x: 0, y: 0}
      snapCorner: 1
      id: unity-grid-and-snap-toolbar
      index: 1
      layout: 1
      size: {x: 0, y: 0}
      sizeOverriden: 0
    - dockPosition: 1
      containerId: overlay-toolbar__top
      floating: 0
      collapsed: 0
      displayed: 1
      snapOffset: {x: 0, y: 0}
      snapOffsetDelta: {x: 0, y: 0}
      snapCorner: 0
      id: unity-scene-view-toolbar
      index: 0
      layout: 1
      size: {x: 0, y: 0}
      sizeOverriden: 0
    - dockPosition: 1
      containerId: overlay-toolbar__top
      floating: 0
      collapsed: 0
      displayed: 0
      snapOffset: {x: 0, y: 0}
      snapOffsetDelta: {x: 0, y: 0}
      snapCorner: 1
      id: unity-search-toolbar
      index: 1
      layout: 1
      size: {x: 0, y: 0}
      sizeOverriden: 0
    - dockPosition: 0
      containerId: overlay-container--left
      floating: 0
      collapsed: 0
      displayed: 1
      snapOffset: {x: 0, y: 0}
      snapOffsetDelta: {x: 0, y: 0}
      snapCorner: 0
      id: unity-transform-toolbar
      index: 0
      layout: 2
      size: {x: 0, y: 0}
      sizeOverriden: 0
    - dockPosition: 0
      containerId: overlay-container--left
      floating: 0
      collapsed: 0
      displayed: 1
      snapOffset: {x: 0, y: 197}
      snapOffsetDelta: {x: 0, y: 0}
      snapCorner: 0
      id: unity-component-tools
      index: 1
      layout: 2
      size: {x: 0, y: 0}
      sizeOverriden: 0
    - dockPosition: 0
      containerId: overlay-container--right
      floating: 0
      collapsed: 0
      displayed: 1
      snapOffset: {x: 67.5, y: 86}
      snapOffsetDelta: {x: 0, y: 0}
      snapCorner: 0
      id: Orientation
      index: 0
      layout: 4
      size: {x: 0, y: 0}
      sizeOverriden: 0
    - dockPosition: 1
      containerId: overlay-container--right
      floating: 0
      collapsed: 0
      displayed: 0
      snapOffset: {x: 0, y: 0}
      snapOffsetDelta: {x: 0, y: 0}
      snapCorner: 0
      id: Scene View/Light Settings
      index: 0
      layout: 4
      size: {x: 0, y: 0}
      sizeOverriden: 0
    - dockPosition: 1
      containerId: overlay-container--right
      floating: 0
      collapsed: 0
      displayed: 0
      snapOffset: {x: 0, y: 0}
      snapOffsetDelta: {x: 0, y: 0}
      snapCorner: 0
      id: Scene View/Camera
      index: 1
      layout: 4
      size: {x: 0, y: 0}
      sizeOverriden: 0
    - dockPosition: 1
      containerId: overlay-container--right
      floating: 0
      collapsed: 0
      displayed: 0
      snapOffset: {x: 0, y: 0}
      snapOffsetDelta: {x: 0, y: 0}
      snapCorner: 0
      id: Scene View/Cloth Constraints
      index: 2
      layout: 4
      size: {x: 0, y: 0}
      sizeOverriden: 0
    - dockPosition: 1
      containerId: overlay-container--right
      floating: 0
      collapsed: 0
      displayed: 0
      snapOffset: {x: 0, y: 0}
      snapOffsetDelta: {x: 0, y: 0}
      snapCorner: 0
      id: Scene View/Cloth Collisions
      index: 3
      layout: 4
      size: {x: 0, y: 0}
      sizeOverriden: 0
    - dockPosition: 1
      containerId: overlay-container--right
      floating: 0
      collapsed: 0
      displayed: 0
      snapOffset: {x: 0, y: 0}
      snapOffsetDelta: {x: 0, y: 0}
      snapCorner: 0
      id: Scene View/Navmesh Display
      index: 4
      layout: 4
      size: {x: 0, y: 0}
      sizeOverriden: 0
    - dockPosition: 1
      containerId: overlay-container--right
      floating: 0
      collapsed: 0
      displayed: 0
      snapOffset: {x: 0, y: 0}
      snapOffsetDelta: {x: 0, y: 0}
      snapCorner: 0
      id: Scene View/Agent Display
      index: 5
      layout: 4
      size: {x: 0, y: 0}
      sizeOverriden: 0
    - dockPosition: 1
      containerId: overlay-container--right
      floating: 0
      collapsed: 0
      displayed: 0
      snapOffset: {x: 0, y: 0}
      snapOffsetDelta: {x: 0, y: 0}
      snapCorner: 0
      id: Scene View/Obstacle Display
      index: 6
      layout: 4
      size: {x: 0, y: 0}
      sizeOverriden: 0
    - dockPosition: 1
      containerId: overlay-container--right
      floating: 0
      collapsed: 0
      displayed: 0
      snapOffset: {x: 0, y: 0}
      snapOffsetDelta: {x: 0, y: 0}
      snapCorner: 0
      id: Scene View/Occlusion Culling
      index: 4
      layout: 4
      size: {x: 0, y: 0}
      sizeOverriden: 0
    - dockPosition: 1
      containerId: overlay-container--right
      floating: 0
      collapsed: 0
      displayed: 0
      snapOffset: {x: 0, y: 0}
      snapOffsetDelta: {x: 0, y: 0}
      snapCorner: 0
      id: Scene View/Physics Debugger
      index: 5
      layout: 4
      size: {x: 0, y: 0}
      sizeOverriden: 0
    - dockPosition: 1
      containerId: overlay-container--right
      floating: 0
      collapsed: 0
      displayed: 0
      snapOffset: {x: 0, y: 0}
      snapOffsetDelta: {x: 0, y: 0}
      snapCorner: 0
      id: Scene View/Scene Visibility
      index: 6
      layout: 4
      size: {x: 0, y: 0}
      sizeOverriden: 0
    - dockPosition: 0
      containerId: Floating
      floating: 1
      collapsed: 0
      displayed: 0
      snapOffset: {x: -231, y: -197}
      snapOffsetDelta: {x: 0, y: 0}
      snapCorner: 3
      id: Scene View/Particles
      index: 0
      layout: 4
      size: {x: 0, y: 0}
      sizeOverriden: 0
    - dockPosition: 1
      containerId: overlay-container--right
      floating: 0
      collapsed: 0
      displayed: 0
      snapOffset: {x: 0, y: 0}
      snapOffsetDelta: {x: 0, y: 0}
      snapCorner: 0
      id: Scene View/Tilemap
      index: 11
      layout: 4
      size: {x: 0, y: 0}
      sizeOverriden: 0
    - dockPosition: 1
      containerId: overlay-container--right
      floating: 0
      collapsed: 0
      displayed: 0
      snapOffset: {x: 0, y: 0}
      snapOffsetDelta: {x: 0, y: 0}
      snapCorner: 0
      id: Scene View/Tilemap Palette Helper
      index: 12
      layout: 4
      size: {x: 0, y: 0}
      sizeOverriden: 0
    - dockPosition: 1
      containerId: overlay-container--right
      floating: 0
      collapsed: 0
      displayed: 0
      snapOffset: {x: 48, y: 48}
      snapOffsetDelta: {x: 0, y: 0}
      snapCorner: 0
      id: Scene View/Visual Effect Timeline Control
      index: 8
      layout: 4
      size: {x: 0, y: 0}
      sizeOverriden: 0
    - dockPosition: 1
      containerId: overlay-container--right
      floating: 0
      collapsed: 0
      displayed: 0
      snapOffset: {x: 48, y: 48}
      snapOffsetDelta: {x: 0, y: 0}
      snapCorner: 0
      id: Scene View/Visual Effect Model
      index: 9
      layout: 4
      size: {x: 0, y: 0}
      sizeOverriden: 0
    - dockPosition: 1
      containerId: overlay-container--right
      floating: 0
      collapsed: 0
      displayed: 0
      snapOffset: {x: 48, y: 48}
      snapOffsetDelta: {x: 0, y: 0}
      snapCorner: 0
      id: Scene View/Visual Effect
      index: 10
      layout: 4
      size: {x: 0, y: 0}
      sizeOverriden: 0
    - dockPosition: 1
      containerId: overlay-container--right
      floating: 0
      collapsed: 0
      displayed: 0
      snapOffset: {x: 48, y: 48}
      snapOffsetDelta: {x: 0, y: 0}
      snapCorner: 0
      id: Scene View/Visual Effect Event Tester
      index: 11
      layout: 4
      size: {x: 0, y: 0}
      sizeOverriden: 0
    - dockPosition: 1
      containerId: overlay-container--right
      floating: 0
      collapsed: 0
      displayed: 0
      snapOffset: {x: 48, y: 48}
      snapOffsetDelta: {x: 0, y: 0}
      snapCorner: 0
      id: APV Overlay
      index: 7
      layout: 4
      size: {x: 0, y: 0}
      sizeOverriden: 0
    - dockPosition: 1
      containerId: overlay-container--right
      floating: 0
      collapsed: 0
      displayed: 0
      snapOffset: {x: 48, y: 48}
      snapOffsetDelta: {x: 0, y: 0}
      snapCorner: 0
      id: Scene View/TrailRenderer
      index: 8
      layout: 4
      size: {x: 0, y: 0}
      sizeOverriden: 0
    m_OverlaysVisible: 1
  m_WindowGUID: 35d5451dbe50f1640a8d05a7f3750486
  m_Gizmos: 1
  m_OverrideSceneCullingMask: 6917529027641081856
  m_SceneIsLit: 1
  m_SceneLighting: 1
  m_2DMode: 0
  m_isRotationLocked: 0
  m_PlayAudio: 0
  m_AudioPlay: 0
  m_Position:
    m_Target: {x: 3.6222024, y: -4.315849, z: -9.95384}
    speed: 2
    m_Value: {x: 3.6222024, y: -4.315849, z: -9.95384}
  m_RenderMode: 0
  m_CameraMode:
    drawMode: 0
    name: Shaded
    section: Shading Mode
  m_ValidateTrueMetals: 0
  m_DoValidateTrueMetals: 0
  m_SceneViewState:
    m_AlwaysRefresh: 1
    showFog: 1
    showSkybox: 1
    showFlares: 1
    showImageEffects: 1
    showParticleSystems: 1
    showVisualEffectGraphs: 1
    m_FxEnabled: 1
  m_Grid:
    xGrid:
      m_Fade:
        m_Target: 0
        speed: 2
        m_Value: 0
      m_Color: {r: 0.5, g: 0.5, b: 0.5, a: 0.4}
      m_Pivot: {x: 0, y: 0, z: 0}
      m_Size: {x: 0, y: 0}
    yGrid:
      m_Fade:
        m_Target: 1
        speed: 2
        m_Value: 1
      m_Color: {r: 0.5, g: 0.5, b: 0.5, a: 0.4}
      m_Pivot: {x: 0, y: 0, z: 0}
      m_Size: {x: 1, y: 1}
    zGrid:
      m_Fade:
        m_Target: 0
        speed: 2
        m_Value: 0
      m_Color: {r: 0.5, g: 0.5, b: 0.5, a: 0.4}
      m_Pivot: {x: 0, y: 0, z: 0}
      m_Size: {x: 0, y: 0}
    m_ShowGrid: 1
    m_GridAxis: 1
    m_gridOpacity: 0.5
  m_Rotation:
    m_Target: {x: -0.07258997, y: -0.81962323, z: 0.10657879, w: -0.55820173}
    speed: 2
    m_Value: {x: -0.07243004, y: -0.8204597, z: 0.106687576, w: -0.5569718}
  m_Size:
    m_Target: 16.585201
    speed: 2
    m_Value: 15.871006
  m_Ortho:
    m_Target: 0
    speed: 2
    m_Value: 0
  m_CameraSettings:
    m_Speed: 1
    m_SpeedNormalized: 0.5
    m_SpeedMin: 0.01
    m_SpeedMax: 2
    m_EasingEnabled: 1
    m_EasingDuration: 0.4
    m_AccelerationEnabled: 1
    m_FieldOfViewHorizontalOrVertical: 60
    m_NearClip: 0.03
    m_FarClip: 10000
    m_DynamicClip: 1
    m_OcclusionCulling: 0
  m_LastSceneViewRotation: {x: 0, y: 0, z: 0, w: 0}
  m_LastSceneViewOrtho: 0
  m_ReplacementShader: {fileID: 0}
  m_ReplacementString: 
  m_SceneVisActive: 1
  m_LastLockedObject: {fileID: 0}
  m_ViewIsLockedToObject: 0
