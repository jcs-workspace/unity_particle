# Fog Plane

Followed the tutorial [Fog Plane Shader Breakdown](https://www.cyanilux.com/tutorials/fog-plane-shader-breakdown/).

This package has two fog plane assets.

- Example 1 (Simple)
- Example 2 (Accurate)
